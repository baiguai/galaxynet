package Engine_Game;

import java.io.*;
import java.util.*;

public class Adjustment
{
    /* PROPERTIES */
    //
        /* ENUMS */
        //
            public static int ADJ_INCREMENT = 1;
            public static int ADJ_SET = 10;
            public static int ADJ_TRAVEL = 20;
            public static int ADJ_MENU = 30;
        //

        private String id = "";
            public String GetId() { return id; }
            public void SetId(String val) { id = val; }

        private int adjustType = 0;
            public int GetAdjustType() { return adjustType; }
            public void SetAdjustType(int val) { adjustType = val; }

        private String adjustAttrib = "";
            public String GetAdjustAttrib() { return adjustAttrib; }
            public void SetAdjustAttrib(String val) { adjustAttrib = val; }

        private String adjustValue = "";
            public String GetAdjustValue() { return adjustValue; }
            public void SetAdjustValue(String val) { adjustValue = val; }

        private boolean removeElement = false;
            public boolean GetRemoveElement() { return removeElement; }
            public void SetRemoveElement(boolean val) { removeElement = val; }

    //


    public Adjustment(String idIn, int adjustTypeIn, String adjustAttribIn, String adjustValueIn, boolean removeElementIn)
    {
        if (idIn.equals(""))
        {
            idIn = Reusable.Functions.GetGUID();
        }

        SetId(idIn);
        SetAdjustType(adjustTypeIn);
        SetAdjustAttrib(adjustAttribIn);
        SetAdjustValue(adjustValueIn);
        SetRemoveElement(removeElementIn);
    }
}
