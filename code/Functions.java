package Reusable;

import java.awt.Color;
import java.util.regex.*;
import java.io.*;
import java.util.*;
import java.util.Random;
import java.util.UUID;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.DocumentBuilder;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import java.io.IOException;
import java.util.Enumeration;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;
import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;
import javax.crypto.spec.IvParameterSpec;
import java.util.regex.Pattern;
import java.text.SimpleDateFormat;
import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.util.stream.Collectors;

/**
    Methods designed to make life easier.
*/
public class Functions
{
    private static Random gRnd = new Random();
    /**
        Puts the game in Debugging mode.
    */
    private static boolean Debugging = true;


    public static void Error(String message)
    {
        System.out.println("\nERROR!\n" + message);
    }

    /**
        Outputs debugging messages.
        <br />
        Parameters:
        <ul>
            <li> file - String <br />
                The file that originated the message.
            </li>
            <li> message - String <br />
                The message to display.
            </li>
        </ul>
    */
    public static void Debug(Class cls, String message)
    {
        String file = cls.getSimpleName();
        if (Debugging)
        {
            System.out.println("Debug (" + file + ") :  " + message);
        }
    }
    /**
        Outputs debugging messages.
        <br />
        Parameters:
        <ul>
            <li> file - String <br />
                The file that originated the message.
            </li>
            <li> message - List&lt;String&gt; <br />
                The list of messages to display.
            </li>
        </ul>
    */
    public static void Debug(Class cls, List<String> messages)
    {
        String file = cls.getSimpleName();
        if (Debugging)
        {
            System.out.println("\nDebug (" + file + "):\n");
            for (String s : messages)
            {
                System.out.println(" - " + s + "\n");
            }
            System.out.println("--------\n");
        }
    }

    /**
        Cleans up IDs.
        <br />
        Parameters:
        <ul>
            <li> id - String <br />
                The ID to clean up.
            </li>
        </ul>
    */
    public static String IDCleanup(String id)
    {
        id = id.replace("[", "").replace("]", "").trim();
        return id;
    }

    // GUID Generator
    public static String GetGUID()
    {
        String output = "";

        UUID uuid = UUID.randomUUID();
        output = uuid.toString();

        return output;
    }

    public static String WrapLines(String input)
    {
        String[] arr = input.split("\n");
        String[] temp = null;
        List<String> output = new ArrayList<String>();
        String outputString = "";
        String line = "";
        int start = 0;
        int wrapPos = Integer.parseInt(GetSetting("Config/Theme.config", "line_width", "-1"));

        if (wrapPos <= 0) return input;
        
        for (String s : arr)
        {
            line = "";
            temp = s.split(" ");

            for (String l : temp)
            {
                if (!line.equals("")) line += " ";

                if ((line + l).length() > wrapPos)
                {
                    output.add(line.trim() + "\n");
                    line = l;
                }
                else
                {
                    line += l;
                }
            }

            if (!line.equals(""))
            {
                output.add(line.trim() + "\n");
            }
        } 

        for (String s : output)
        {
            outputString += s;
        }

        return outputString;
    }

    // Case insensitive match - or contains.
    public static boolean Match(String source1, String source2)
    {
        boolean isMatch = false;
        String[] arr = GetArray(source2);

        for (String itm: arr)
        {
            if (source1.equalsIgnoreCase(itm) ||
                source1.toLowerCase().contains(source2.toLowerCase()))
            {
                isMatch = true;
                break;
            }
        }

        return isMatch;
    }

    public static boolean MatchWord(String source1, String source2)
    {
        boolean isMatch = false;
        String[] arr = GetArray(source2);

        for (String itm: arr)
        {
            if (source1.equalsIgnoreCase(itm) ||
                source1.toLowerCase().equals(source2.toLowerCase()))
            {
                isMatch = true;
                break;
            }
        }

        return isMatch;
    }

    // Wildcard Match
    public static boolean RegMatch(String target, String input)
    {
        boolean isMatch = false;

        target = target.toLowerCase();
        input = input.toLowerCase();

        String[] arr = GetArray(target);

        input = input.trim();

        for (String sTarget : arr)
        {
            if (Pattern.matches(WildcardToRegex(sTarget), input))
            {
                isMatch = true;
                break;
            }
        }

        return isMatch;
    }

    public static String WildcardToRegex(String wildcard){
        StringBuffer s = new StringBuffer(wildcard.length());
        s.append('^');
        for (int i = 0, is = wildcard.length(); i < is; i++) {
            char c = wildcard.charAt(i);
            switch(c) {
                case '*':
                    s.append(".*");
                    break;
                case '?':
                    s.append(".");
                    break;
                    // escape special regexp-characters
                case '(': case ')': case '[': case ']': case '$':
                case '^': case '.': case '{': case '}': case '|':
                case '\\':
                    s.append("\\");
                    s.append(c);
                    break;
                default:
                    s.append(c);
                    break;
            }
        }
        s.append('$');
        return(s.toString());
    }

    public static String[] GetArray(String input)
    {
        input = input.replace("|", ",");
        String[] arr = input.split(",");
        return arr;
    }

    public static void OutputRaw(String message)
    {
        System.out.println(message);
    }
    public static void Output(List<String> message)
    {
        int maxLine = 80;
        String div = "";
        for (int ix = 0; ix < maxLine; ix++) { div += "-"; }

        System.out.println("\n\n");
        for (String s : message)
        {
            s = s.replace("_hr_", div);
            System.out.println(s);
        }
        System.out.println("\n\n");
    }
    public static void Output(String message)
    {
        System.out.println("\n\n" + message + "\n\n");
    }

    // Convert to Table name
    public static String ToTable(String name)
    {
        name = name.trim().toLowerCase();
        return name.substring(0, 1).toUpperCase() + name.substring(1);
    }

    // True Test from String
    public static boolean TrueString(String checkVal)
    {
        boolean output = false;
        if (checkVal.equals("")) return output;

        checkVal = checkVal.trim().toLowerCase();

        if (checkVal.equals("true") ||
            checkVal.equals("1"))
        {
            output = true;
        }

        return output;
    }

    public static int ToInt(String valueIn)
    {
        return ToInt(valueIn, 0);
    }
    public static int ToInt(String valueIn, int defaultValue)
    {
        int output = defaultValue;

        try
        {
            output = Integer.parseInt(valueIn);
        }
        catch (Exception ex) {}

        return output;
    }

    public static int Rounded(int val, int roundTo)
    {
        int rounded = ((val + (roundTo - 1)) / roundTo ) * roundTo;
        return rounded;
    }


    // Array to string.
    public static String ArrayToString(String[] array, int startIndex, String delimiter)
    {
        String output = "";

        for (int ix = startIndex; ix < array.length; ix++)
        {
            if (!output.equals("")) output += delimiter;
            output += array[ix];
        }

        return output;
    }

    // Random Int
    // if min = -1, only use the max value
    public static int RandomInt(int min, int max)
    {
        if (min == 0 && max == 0) return 0;
        int seedStep = gRnd.nextInt(100);
        int tmp = 0;
        int rndAmt = 0;

        // Ensure the output is random - since each
        // call creates a new random object
        for (int i = 0; i <= seedStep; i++)
        {
            tmp = gRnd.nextInt(100);
        }

        int output = -1;

        if (max != 0)
        {
            if (min >= max)
            {
                max = min;
                min = -1;
            }
            if (min < 0) min = 0;

            if (min > max) min = max;

            rndAmt = max - min;
            if (rndAmt < 1) rndAmt = 1;
        }

        if (max ==  0)
        {
            output = gRnd.nextInt(min);
        }
        else
        {
            output = gRnd.nextInt(rndAmt) + min;
        }

        return output;
    }

    // Sql Cleanup.
    public static String SqlCleanup(String input)
    {
        if (input.equals("")) return input;

        input = input.replaceAll("'", "''");

        return input;
    }

    // Clear the console
    public static void ClearConsole()
    {
        try
        {
            final String os = System.getProperty("os.name");

            if (os.contains("Windows"))
            {
                new ProcessBuilder("cmd", "/c", "cls").inheritIO().start().waitFor();
            }
            else
            {
                System.out.print("\033[H\033[2J");
            }
        }
        catch (final Exception e)
        {
            //  Handle any exceptions.
        }
    }

    // Get Setting
    /*
        -   file
            This parameter is the file path without extension.
            It is relative to the application root.
            It must have a .properties extension.
    */
    public static String GetSetting(String file, String key, String defVal)
    {
        Properties prop = new Properties();
        String value = "";

        try
        {
            Properties props = new Properties();
            String propFile = file;
            FileInputStream inputStream = new FileInputStream(propFile);

            if (inputStream != null)
            {
                prop.load(inputStream);
                value = prop.getProperty(key);
            }
            else
            {
                key = "";
                //throw new FileNotFoundException("Config file not found: " + propFile);
            }
        }
        catch (Exception ex)
        {
            value = "";
        }

        if (value == null) value = "";

        if (value.equals("")) value = defVal;

        return value;
    }
    public static int GetSetting(String file, String key, int defVal)
    {
        Properties prop = new Properties();
        String tmp = "";
        int value = defVal;

        try
        {
            Properties props = new Properties();
            String propFile = file;
            FileInputStream inputStream = new FileInputStream(propFile);

            if (inputStream != null)
            {
                prop.load(inputStream);
                tmp = prop.getProperty(key);

                try
                {
                    value = Integer.parseInt(tmp);
                }
                catch (Exception exInt)
                {
                    value = defVal;
                }
            }
            else
            {
                key = "";
                //throw new FileNotFoundException("Config file not found: " + propFile);
            }
        }
        catch (Exception ex)
        {
            value = defVal;
        }

        return value;
    }
    public static void SetSetting(String file, String key, int val)
    {
        SetSetting(file, key, val + "");
    }
    public static void SetSetting(String file, String key, String val)
    {
        File f = new File(file);
        boolean found = false;

        try
        {
            if (!f.exists()) f.createNewFile();
        }
        catch (Exception ex) {}

        List<String> data = ReadFile(file);
        String s = "";

        for (int ix = 0; ix < data.size(); ix++)
        {
            s = data.get(ix);
            if (s.length() > key.length() &&
                s.indexOf(key + "=") >= 0)
            {
                found = true;
                data.set(ix, key + "=" + val);
                break;
            }
        }

        if (!found)
        {
            data.add(key + "=" + val);
        }

        if (data.size() > 0)
        {
            WriteToFile(file, data);
        }
    }


    // Get Formatted Date
    public static String GetDate()
    {
        String output = "";
        
        Date today = new Date();
        SimpleDateFormat TS_FORMAT = new SimpleDateFormat("yyyy.MM.dd_HH.mm.ss");
        output = TS_FORMAT.format(today);

        return output;
    }



    public static String PadNumber(int number, int pad, String padChar) {
        String output = "";
        if (padChar.equals("")) padChar = "0";
        String sNum = number + "";

        while (sNum.length() < pad)
        {
            sNum = padChar + sNum;
        }

        output = sNum;

        return output;
    }

    public static String TitleCase(String text)
    {
        String WORD_SEPARATOR = " ";

        if (text == null || text.isEmpty()) {
            return text;
        }
     
        return Arrays
          .stream(text.split(WORD_SEPARATOR))
          .map(word -> word.isEmpty()
            ? word
            : Character.toTitleCase(word.charAt(0)) + word
              .substring(1)
              .toLowerCase())
          .collect(Collectors.joining(WORD_SEPARATOR));
    }



    public static String SafeFilePath(String path)
    {
        String output = path;

        File f = new File(path);

        if (!f.exists()) output = "";

        return output;
    }

    public static List<String> ListFiles(String path, String extension)
    {
        List<String> files = new ArrayList<String>();

        try
        {
            // Iterate through the plugins dir
            File[] fileList = new File(path).listFiles();

            if (fileList == null) return files;

            for (File file : fileList)
            {
                if (extension.equals("") || !file.isDirectory() && file.getName().indexOf(extension) >= 0)
                {
                    files.add(file.getName());
                }
            }
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
        }

        return files;
    }

    public static List<String> ListDirs(String path)
    {
        List<String> files = new ArrayList<String>();

        try
        {
            // Iterate through the plugins dir
            File[] fileList = new File(path).listFiles();

            if (fileList == null) return files;

            for (File file : fileList)
            {
                if (file.isDirectory())
                {
                    files.add(file.getName());
                }
            }
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
        }

        return files;
    }

    public static List<String> ListDirs(String path, String exclude)
    {
        List<String> files = new ArrayList<String>();

        try
        {
            // Iterate through the plugins dir
            File[] fileList = new File(path).listFiles();

            if (fileList == null) return files;

            for (File file : fileList)
            {
                if (file.isDirectory() && file.getName().toLowerCase().indexOf(exclude) < 0)
                {
                    files.add(file.getName());
                }
            }
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
        }

        return files;
    }

    public static List<String> ListJarFiles(String jar, String extension)
    {
        List<String> files = new ArrayList<String>();

        try
        {
            JarFile jarFile = new JarFile(jar);
            Enumeration allEntries = jarFile.entries();

            while (allEntries.hasMoreElements()) {
                JarEntry entry = (JarEntry) allEntries.nextElement();
                String name = entry.getName();

                if (name.indexOf(extension) >= 0)
                {
                    files.add(name);
                }
            }
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
        }

        return files;
    }

    public static boolean DeleteFile(String path)
    {
        boolean success = true;

        try
        {
            File f = new File(path);
            if (!f.isDirectory())
            {
                f.delete();
            }
        }
        catch (Exception ex)
        {
            success = false;
        }

        return success;
    }

    public static boolean ClearFolder(String path, String extension)
    {
        boolean success = true;
        List<String> files = new ArrayList<String>();

        try
        {
            // Iterate through the plugins dir
            File[] fileList = new File(path).listFiles();

            if (fileList == null) return true;

            for (File file : fileList)
            {
                if (!file.isDirectory())
                {
                    if (extension.equals("") || file.getName().indexOf(extension) >= 0)
                    {
                        file.delete();
                    }
                }
            }
        }
        catch (Exception ex)
        {
            success = false;
        }

        return success;
    }

    public static void MakeFile(String file)
    {
        try
        {
            File f = new File(file);
            if (!f.exists()) f.createNewFile();
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
        }
    }

    public static List<String> ReadFile(String file)
    {
        List<String> inp = new ArrayList<String>();
        String line;

        try
        {
            try (BufferedReader br = new BufferedReader(new FileReader(file))) {
                while ((line = br.readLine()) != null) {
                    inp.add(line);
                }
            }
        }
        catch (Exception ex)
        {
            // ex.printStackTrace();
        }

        return inp;
    }

    public static void WriteToFile(String file, String contents)
    {
        File f = new File(file);

        try
        {
            if (!f.exists())
            {
                f.createNewFile();
            }

            PrintWriter writer = new PrintWriter(file, "UTF-8");

            writer.println(contents);

            writer.close();
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
            Functions.Output("File: " + file);
        }
    }
    public static void WriteToFile(String file, List<String> contents)
    {
        File f = new File(file);

        try
        {
            if (!f.exists())
            {
                f.createNewFile();
            }

            PrintWriter writer = new PrintWriter(file, "UTF-8");

            for (String s : contents)
            {
                writer.println(s);
            }

            writer.close();
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
            Functions.Output("File: " + file);
        }
    }
    public static void OverwriteToFile(String file, List<String> contents)
    {
        File f = new File(file);
        if (f.exists())
        {
            f.delete();
        }

        try
        {
            PrintWriter writer = new PrintWriter(file, "UTF-8");

            for (String s : contents)
            {
                writer.println(s);
            }

            writer.close();
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
            Functions.Output("File: " + file);
        }
    }

    public static void AppendToFile(String file, String contents)
    {
        try
        {
            FileWriter fw = new FileWriter(file, true); //the true will append the new data
            fw.write(contents + "\n");
            fw.close();
        }
        catch(IOException ioe)
        {
            System.err.println("IOException: " + ioe.getMessage());
        }
    }

    public static Document GetDoc(String file)
    {
        Document doc = null;
        File xmlFile = null;
        DocumentBuilderFactory dbFactory = null;
        DocumentBuilder dBuilder = null;

        try
        {
            xmlFile = new File(file);
            dbFactory = DocumentBuilderFactory.newInstance();
            dBuilder = dbFactory.newDocumentBuilder();
            doc = dBuilder.parse(xmlFile);

            doc.getDocumentElement().normalize();
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
        }

        return doc;
    }


    // Encryption / Decryption
    private static Cipher GenerateCipher()
    {
        Cipher cipher = null;
        byte[] keyBytes = ("60061c8c2de01ce9").getBytes();
        byte[] ivBytes = ("5188b8b8").getBytes();

        try
        {
            // wrap key data in Key/IV specs to pass to cipher
            SecretKeySpec key = new SecretKeySpec(keyBytes, "DES");
            IvParameterSpec ivSpec = new IvParameterSpec(ivBytes);
            // create the cipher with the algorithm you choose
            // see javadoc for Cipher class for more info, e.g.
            cipher = Cipher.getInstance("DES/CBC/PKCS5Padding");

            cipher.init(Cipher.ENCRYPT_MODE, key, ivSpec);
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
        }

        return cipher;
    }

    public static String EncryptString(String inputString)
    {
        byte[] encrypted = null;

        try
        {
            Cipher cipher = GenerateCipher();

            byte[] input = inputString.getBytes();
            encrypted = new byte[cipher.getOutputSize(input.length)];
            int enc_len = cipher.update(input, 0, input.length, encrypted, 0);
            enc_len += cipher.doFinal(encrypted, enc_len);
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
        }

        return encrypted.toString();
    }

    public static Color ParseColor(String colorString)
    {
        Color defaultColor = new Color(0,0,0,255);
        Color c = defaultColor;
        String[] arr = colorString.split(",");

        if (arr.length != 4) return defaultColor;

        try
        {
            c = new Color(Integer.parseInt(arr[0]),
                          Integer.parseInt(arr[1]),
                          Integer.parseInt(arr[2]),
                          Integer.parseInt(arr[3]));
        }
        catch (Exception ex)
        {
            c = defaultColor;
        }

        return c;
    }

    public static boolean StringToBoolean(String input)
    {
        boolean output = false;
        input = input.trim().toLowerCase();

        if (input.equals("1") || input.equals("y") || input.equals("true"))
        {
            output = true;
        }

        return output;
    }



    public static int GetCenterCoord(int coord, int maxCoord)
    {
        return coord - (maxCoord / 2);
    }

    public static int GetCoordFromCenter(int coord, int maxCoord)
    {
        return (maxCoord / 2) + coord;
    }


    public static BufferedImage GetImage(String path)
    {
        BufferedImage output = null;
        File f = new File(path);

        try
        {
            output = ImageIO.read(f);
        }
        catch (Exception ex)
        {
            System.out.println("Can't load image: " + path);
            ex.printStackTrace();
        }

        return output;
    }
}
