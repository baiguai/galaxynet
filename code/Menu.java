package Engine_Game;

import java.awt.Color;
import java.io.*;
import java.util.*;
import java.awt.Graphics2D;
import java.awt.image.*;
import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.awt.event.*;
import Reusable.Functions;

public class Menu
{
    /* PROPERTIES */
    //
        /* ENUMS */
        //
            public static int MNU_UPGRADE = 1;
        //

        public static boolean ValidMenuType(int val)
        {
            boolean output = true;

            if (val != MNU_UPGRADE)
            {
                output = false;
            }

            return output;
        }

        private int menuType = 1;
            public int GetMenuType() { return menuType; }
            public void SetMenuType(int val) { menuType = val; }

        private List<MenuItem> menuItems = null;
            public List<MenuItem> GetMenuItems() { if (menuItems == null) menuItems = new ArrayList<MenuItem>(); return menuItems; }
            public void SetMenuItems(List<MenuItem> val) { menuItems = val; }
            public void AddMenuItem(MenuItem val) { GetMenuItems().add(val); }
            public void ClearMenuItems() { menuItems = null; }

        private int thumbSize = 32;
            public int GetThumbSize() { return thumbSize; }
            public void SetThumbSize(int val) { thumbSize = val; }

        private int padding = 8;
            public int GetPadding() { return padding; }
            public void SetPadding(int val) { padding = val; }

        private int colCount = 4;
            public int GetColCount() { return colCount; }
            public void SetColCount(int val) { colCount = val; }

        private Color background = new Color(255,255,255,255);
            public Color GetBackground() { return background; }
            public void SetBackground(Color val) { background = val; }

        private Color foreground = new Color(0,0,0,255);
            public Color GetForeground() { return foreground; }
            public void SetForeground(Color val) { foreground = val; }

        private int selectedItem = 0;
            public int GetSelectedItem() { return selectedItem; }
            public void SetSelectedItem(int val) { selectedItem = val; }
            public void AddSelectedItemX(int val)
            {
                int s = GetSelectedItem();

                s = s + val;

                if (val > 0)
                {
                    if (s >= GetMenuItems().size()) s = 0;
                }

                if (val < 0)
                {
                    if (s < 0) s = GetMenuItems().size() - 1;
                }

                SetSelectedItem(s);
            }
            public void AddSelectedItemY(int val)
            {
                int s = GetSelectedItem();

                s = s + (val * GetColCount());

                if (val > 0)
                {
                    if (s >= GetMenuItems().size()) s = GetMenuItems().size() - 1;
                }

                if (val < 0)
                {
                    if (s < 0) s = 0;
                }

                SetSelectedItem(s);
            }
    //


    public Menu(int menuTypeIn)
    {
        SetMenuType(menuTypeIn);

        SetupClasses();
    }

    private void SetupClasses()
    {
        if (GetMenuType() == MNU_UPGRADE) SetupUpgrade();
    }

    private void SetupUpgrade()
    {
        String upgDir = "library/menus/upgrade";
        String settings = upgDir + "/settings.config";
        List<String> dirs = Functions.ListDirs(upgDir);
        BufferedImage img = null;
        File f = null;
        String path = "";
        MenuItem itm = null;

        SetBackground(Functions.ParseColor(Functions.GetSetting(settings, "background", "255,255,255,255")));
        SetForeground(Functions.ParseColor(Functions.GetSetting(settings, "foreground", "0,0,0,255")));
        SetThumbSize(Functions.GetSetting(settings, "thumb_size", 32));
        SetPadding(Functions.GetSetting(settings, "padding", 8));
        SetColCount(Functions.GetSetting(settings, "col_count", 4));

        ClearMenuItems();

        for (String d : dirs)
        {
            path = upgDir + "/" + d;

            f = new File(path);

            if (f.exists())
            {
                itm = new MenuItem(path);
                AddMenuItem(itm);
            }
        }
    }



    public void KeyPressed(int keyCode)
    {}

    public void KeyReleased(int keyCode)
    {
        if (keyCode == KeyEvent.VK_LEFT)
        {
            AddSelectedItemX(-1);
        }

        if (keyCode == KeyEvent.VK_RIGHT)
        {
            AddSelectedItemX(1);
        }

        if (keyCode == KeyEvent.VK_UP)
        {
            AddSelectedItemY(-1);
        }

        if (keyCode == KeyEvent.VK_DOWN)
        {
            AddSelectedItemY(1);
        }
    }


    public void GameUpdate()
    {}

    public void GameDraw(Graphics2D g)
    {
        g.setColor(GetBackground());
        int totalWidth = (GetThumbSize() * GetColCount()) + (GetPadding() * (GetColCount() - 1));
        int x = (Game.GAMEWIDTH / 2) - (totalWidth / 2);
        int y = (Game.GAMEHEIGHT / 2) - ((GetMenuItems().size() / GetColCount()) / 2);
        int curX = 0;
        int rItm = 0;

        g.fillRect(0, 0, Game.GAMEWIDTH, Game.GAMEHEIGHT);

        g.setColor(GetForeground());

        for (int i = 0; i < GetMenuItems().size(); i++)
        {
            if (rItm > GetColCount())
            {
                curX = 0;
                y += GetThumbSize() + GetPadding();
            }

            GetMenuItems().get(i).GameDraw(g, x + curX, y);

            if (GetSelectedItem() == i)
            {
                g.drawRect((x + curX) - (GetPadding() / 2), (y - (GetPadding() / 2)), (GetThumbSize() + GetPadding()), (GetThumbSize() + GetPadding()));
            }

            curX += GetThumbSize() + GetPadding();
            rItm = rItm++;
        }

        g.setColor(new Color(0,0,0,255));
    }
}
