package Engine_Kernel;

import javax.swing.JFrame;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Point;
import java.awt.Toolkit;
import java.awt.Graphics2D;
import java.awt.Graphics;
import java.awt.Dimension;
import java.awt.RenderingHints;
import java.awt.image.*;
import java.awt.Image;
import java.awt.event.*;
import java.util.*;
import java.io.*;
import java.io.IOException;
import javax.imageio.ImageIO;
import java.net.URL;
import java.lang.Thread;
import Reusable.Functions;

/*

    General Notes:
    ----------------------------------------------------------------------------
    Indentation
        The weird indentation that goes beyond keeping the code clean is
        intended for indent-based folding. I code in VIM set to indented folds.
        I like looking at small amounts of code at a time (unless I need to see
        the code in its entirety) so I indent chunks of code so I can fold it.
        Properties are where you will see this most often.
    ----------------------------------------------------------------------------

*/

public class Main
{
    public static void main(String[] args)
    {
        // Perform initial validation
        // - Folders
        // - System reqs etc
        // ---------------------------------------------------------------------
        CreateFolders();
        // ---------------------------------------------------------------------


        // Setup the Window
        // Prepping to make it full screen
        // ---------------------------------------------------------------------
        String gameName = "Galaxynet";
        JFrame window = new JFrame(gameName);
        window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        // SetIcon(window);

        window.getContentPane().setBackground(Color.BLACK);
        window.setExtendedState(JFrame.MAXIMIZED_BOTH); 
        window.setUndecorated(true);
        window.setVisible(true);

        window.pack();

        while ((window.getSize().width < 1 || window.getSize().height < 1) ||
               window.getContentPane() == null)
        {
            // wait for the jframe to become visible
            try
            {
                Thread.sleep(3000);
            }
            catch (Exception ex) {}
        }

        // Load the Game Contents
        window.setContentPane(new GamePanel(window.getSize().width, window.getSize().height));

        // Hide the cursor
        // HideCursor(window);

        // Re-Pack the window
        window.pack();
        // ---------------------------------------------------------------------
    }


    private static void SetIcon(JFrame window)
    {
        Image img;
        File imgF = new File("Assets/Images/Icon_Main.png");
        if (imgF == null) 
        {
            System.err.println("Couldn't find file: assets/images/icon_main.png");
        } 
        else 
        {
            try {
                img = ImageIO.read(imgF);
                window.setIconImage(img);
            } 
            catch (IOException ex) 
            {
                //ex.printStackTrace();
            }
        }
    }

    private static void HideCursor(JFrame window)
    {
        BufferedImage cursorImg = new BufferedImage(16, 16, BufferedImage.TYPE_INT_ARGB);
        Cursor blankCursor = Toolkit.getDefaultToolkit().createCustomCursor(cursorImg, new Point(0, 0), "blank cursor");
        window.getContentPane().setCursor(blankCursor);
    }

    private static void CreateFolders()
    {
        String dir = "";
        String fil = "";
        String conf = "config/settings.config";
        File f = null;
        List<String> fileData = new ArrayList<String>();
        int min = 0;
        int max = 0;

        min = Functions.GetSetting(conf, "fuel_min", 100);
        max = Functions.GetSetting(conf, "fuel_max", 500);

        dir = "data";
        f = new File(dir);
        if (!f.exists()) f.mkdir();

        fil = "data/game.dat";
        f = new File(fil);
        fileData = new ArrayList<String>();
        fileData.add("posx=0");
        fileData.add("posy=0");
        fileData.add("levelid=");
        fileData.add("life=100");
        fileData.add("points=0");
        fileData.add("minerals=0");
        fileData.add("fuel=" + Functions.RandomInt(min, max));
        fileData.add("fuel_burn_rate=" + Functions.GetSetting(conf, "fuel_burn_rate", 50));
        fileData.add("centerx=0");
        fileData.add("centery=0");
        fileData.add("sector=");
        if (!f.exists()) Functions.WriteToFile(fil, fileData);

        dir = "data/sectors";
        f = new File(dir);
        if (!f.exists()) f.mkdir();

        dir = "data/levels";
        f = new File(dir);
        if (!f.exists()) f.mkdir();

        dir = "data/adjustments";
        f = new File(dir);
        if (!f.exists()) f.mkdir();

        dir = "data/travel";
        f = new File(dir);
        if (!f.exists()) f.mkdir();
    }
}
