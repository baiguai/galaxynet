package Engine_Game;

public class Move
{
    /* PROPERTIES */
    //
        /* ENUMS */
        //
            public static int MV_TYPE_STD = 1;
            public static int MV_TYPE_EN_STALK = 10;
        //

        private Coord coord = null;
            public Coord GetCoord() { return coord; }
            public void SetCoord(Coord val) { coord = val; }

        private boolean reverse = false;
            public boolean GetReverse() { return reverse; }
            public void SetReverse(boolean val) { reverse = val; }

        private boolean wrap = false;
            public boolean GetWrap() { return wrap; }
            public void SetWrap(boolean val) { wrap = val; }

        private boolean randomOnWrap = false;
            public boolean GetRandomOnWrap() { return randomOnWrap; }
            public void SetRandomOnWrap(boolean val) { randomOnWrap = val; }

        private int speedX = 3;
            public int GetSpeedX() { return speedX; }
            public void SetSpeedX(int val) { speedX = val; }
            public void AddSpeedX(int val) { speedX += val; }

        private int speedY = 3;
            public int GetSpeedY() { return speedY; }
            public void SetSpeedY(int val) { speedY = val; }
            public void AddSpeedY(int val) { speedY += val; }

        private int moveType = 1;
            public int GetMoveType() { return moveType; }
            public void SetMoveType(int val) { moveType = val; }

        private int movingX = 0;
            public int GetMovingX() { return movingX; }
            public void SetMovingX(int val) { if (val > 1) val = 1; if (val < -1) val = -1; if (GetReverse()) val = -(val); movingX = val; }

        private int movingY = 0;
            public int GetMovingY() { return movingY; }
            public void SetMovingY(int val) { if (val > 1) val = 1; if (val < -1) val = -1; if (GetReverse()) val = -(val); movingY = val; }
    //


    public Move(int widthIn, int heightIn, int gameWidthIn, int gameHeightIn)
    {
        Coord c = new Coord(widthIn, heightIn, gameWidthIn, gameHeightIn);
        SetCoord(c);
    }
}
