package Engine_Game;

import java.util.*;
import java.lang.Math.*;
import Reusable.Functions;

public class Collision
{
    /* PROPERTIES */
    //
        /* ENUMS */
        //
            public static int COL_OBJECT = 1;
            public static int COL_ADJUST = 2;
            public static int COL_ENEMY = 3;
        //

        private Element parent;
        public Element GetParent() { return parent; }
        public void SetParent(Element val) { parent = val; }

        private int type = 1;
            public int GetType() { return type; }
            public void SetType(int val) { type = val; }

        private List<Element> gameObjects = null;
        public List<Element> GetGameObjects() { if (gameObjects == null) gameObjects = new ArrayList<Element>(); return gameObjects; }
        public void SetGameObjects(List<Element> val) { gameObjects = val; }
        public void AddGameObject(Element val)
        {
            if (!GameObjectExists(val.GetId()))
            {
                GetGameObjects().add(val);
            }
        }
        public void RemoveGameObject(String val)
        {
            List<Element> output = new ArrayList<Element>();

            for (Element e : GetGameObjects())
            {
                if (!e.GetId().equals(val))
                {
                    output.add(e);
                }
            }

            SetGameObjects(output);
        }
        public void ClearGameObjects() { gameObjects = null; }
        public boolean GameObjectExists(String val)
        {
            boolean output = false;

            for (Element e : GetGameObjects())
            {
                if (e.GetId().equals(val))
                {
                    output = true;
                    break;
                }
            }

            return output;
        }
    //


    public Collision(Element parentIn)
    {
        SetParent(parentIn);
    }


    public CollisionDetail CollisionCheck(List<Element> gameObjectsIn)
    {
        SetGameObjects(gameObjectsIn);
        return CollisionCheck();
    }
    /*
        For some reason this works - but if a collision is detected
        and the player goes the opposite direction a little, then back
        it doesn't see the collision.
        Why??
    */
    public CollisionDetail CollisionCheck()
    {
        CollisionDetail output = null;
        Move selfMove = null;
        Coord selfCoord = null;
        Move objectMove = null;
        Coord objectCoord = null;
        boolean collide = false;
        int selfLeft = 0;
        int selfTop = 0;
        int selfRight = 0;
        int selfBottom = 0;
        int objectLeft = 0;
        int objectTop = 0;
        int objectRight = 0;
        int objectBottom = 0;

        selfMove = GetParent().GetMove();
        selfCoord = selfMove.GetCoord();

        // Get the parent location values
        selfLeft = GetX(selfCoord.GetLeft(), selfMove);
        selfRight = GetX(selfCoord.GetRight(), selfMove);
        selfTop = GetY(selfCoord.GetTop(), selfMove);
        selfBottom = GetY(selfCoord.GetBottom(), selfMove);

        for (Element e : GetGameObjects())
        {
            if (e.GetId().equals(GetParent().GetId())) continue;
            // if (e.GetAdjustments().size() < 1) continue;

            objectMove = e.GetMove();
            objectCoord = objectMove.GetCoord();

            objectLeft = GetX(objectCoord.GetLeft(), objectMove);
            objectRight = GetX(objectCoord.GetRight(), objectMove);
            objectTop = GetY(objectCoord.GetTop(), objectMove);
            objectBottom = GetY(objectCoord.GetBottom(), objectMove);

            if (HasCollided(selfMove, selfLeft, selfRight, objectLeft, objectRight, selfTop, selfBottom, objectTop, objectBottom))
            {
                output = new CollisionDetail();
                output.SetCollidedWith(e);

                // DETERMINE THE SIDE THAT HIT
                output.SetCollidedX(GetXCollision(selfLeft, selfRight, objectLeft, objectRight, selfMove, objectMove));
                output.SetCollidedY(GetYCollision(selfTop, selfBottom, objectTop, objectBottom, selfMove, objectMove));

                break;
            }
        }

        return output;
    }


    /* HELPERS */
    private int GetX(int x, Move m)
    {
        int output = x;
        int spd = m.GetSpeedX();

        if (m.GetMovingX() < 0) { output -= spd; }
        if (m.GetMovingX() > 0) { output += spd; }

        return output;
    }
    private int GetY(int y, Move m)
    {
        int output = y;
        int spd = m.GetSpeedY();

        if (m.GetMovingY() < 0) { output -= spd; }
        if (m.GetMovingY() > 0) { output += spd; }

        return output;
    }

    private boolean HasCollided(Move selfMove, int selfLeft, int selfRight, int objectLeft, int objectRight, int selfTop, int selfBottom, int objectTop, int objectBottom)
    {
        int padding = 4;
        boolean hitX = false;
        boolean hitY = false;
        boolean output = false;
        // Add padding to the object size
        objectLeft -= padding;
        objectRight += padding;
        objectTop -= padding;
        objectBottom += padding;

        if (
            (selfRight < objectRight && selfRight >= objectLeft && selfMove.GetMovingX() > 0) ||
            (selfLeft > objectLeft && selfLeft <= objectRight && selfMove.GetMovingX() < 0)
        )
        {
            if (
                selfBottom >= objectTop + padding &&
                selfTop <= objectBottom - padding
            )
            {
                hitX = true;
            }
        }

        if (
            (selfBottom < objectBottom && selfBottom >= objectTop && selfMove.GetMovingY() > 0) ||
            (selfTop > objectTop && selfTop <= objectBottom && selfMove.GetMovingY() < 0)
        )
        {
            if (
                selfRight >= objectLeft + padding &&
                selfLeft <= objectRight - padding
            )
            {
                hitY = true;
            }
        }

        if (hitX || hitY) output = true;


/*
        if (
            selfRight >= objectLeft &&
            selfLeft <= objectRight &&
            selfBottom >= objectTop &&
            selfTop <= objectBottom
        )
        {
            output = true;
        }
*/

        return output;
    }

    private int GetXCollision(int selfLeft, int selfRight, int objectLeft, int objectRight, Move selfMove, Move objectMove)
    {
        int output = 0;

        // Check collided w/ the right side of the object
        if (selfMove.GetMovingX() < 0 || objectMove.GetMovingX() > 0)
        {
            output = 1;
        }
        // Check collided w/ the left side of the object
        if (selfMove.GetMovingX() > 0 || objectMove.GetMovingX() < 0)
        {
            output = -1;
        }

        return output;
    }
    private int GetYCollision(int selfTop, int selfBottom, int objectTop, int objectBottom, Move selfMove, Move objectMove)
    {
        int output = 0;

        // Check collided w/ the bottom side of the object
        if (selfMove.GetMovingY() < 0 || objectMove.GetMovingY() > 0)
        {
            output = 1;
        }
        // Check collided w/ the top side of the object
        if (selfMove.GetMovingY() > 0 || objectMove.GetMovingY() < 0)
        {
            output = -1;
        }

        return output;
    }
}
