package Engine_Game;

import java.awt.Color;
import java.awt.Graphics2D;
import java.util.*;

public class StarField
{
    /* PROPERTIES */
    //
        private List<Element> stars = null;
            public List<Element> GetStars() { if (stars == null) stars = new ArrayList<Element>(); return stars; }
            public void SetStars(List<Element> val) { stars = val; }
            public void AddStar(Element val) { GetStars().add(val); }
            public void ClearStars() { stars = null; }

        private int width = 0;
            public int GetWidth() { return width; }
            public void SetWidth(int val) { width = val; }

        private int height = 0;
            public int GetHeight() { return height; }
            public void SetHeight(int val) { height = val; }
    //


    public StarField(int speedXIn, int speedYIn, int gameWidth, int gameHeight)
    {
        List<Move> lst = new ArrayList<Move>();
        Move m = null;
        Element e = null;
        int layers = 3;
        int starCnt = 200;
        int starSz = 2;
        int szAdj = 0;
        int colorVal = 100;

        SetWidth(gameWidth);
        SetHeight(gameHeight);

        // Ensure the stars move slightly slower than the level objects
        speedXIn--;
        speedYIn--;

        for (int i = 0; i < layers; i++)
        {
            for (int s = 0; s <= starCnt; s++)
            {
                starSz = Reusable.Functions.RandomInt(1, 3) - szAdj;
                e = new Element("", Element.ELEM_STAR, "star", "", starSz, starSz, gameWidth, gameHeight, false);
                e.SetElementType(Element.ELEM_STAR);
                e.SetColor(new Color(colorVal, colorVal, colorVal));
                e.GetMove().SetReverse(true);
                e.GetMove().SetWrap(true);
                e.GetMove().SetRandomOnWrap(true);
                e.GetMove().SetSpeedX(speedXIn);
                e.GetMove().SetSpeedY(speedYIn);
                e.GetMove().GetCoord().SetLeft(Reusable.Functions.RandomInt(0, GetWidth()));
                e.GetMove().GetCoord().SetTop(Reusable.Functions.RandomInt(0, GetHeight()));
                AddStar(e);
            }

            starCnt-=50;
            colorVal-=50;
            if (colorVal < 0) colorVal = 10;
            speedXIn--;
            speedYIn--;
            // Ensure no stars are stationary
            if (speedXIn <= 0) speedXIn = 1;
            if (speedYIn <= 0) speedYIn = 1;
            szAdj--;
        }
    }


    public void SetMovingX(int moving)
    {
        for (Element e : GetStars())
        {
            e.SetMovingX(moving);
        }
    }
    public void SetMovingY(int moving)
    {
        for (Element e : GetStars())
        {
            e.SetMovingY(moving);
        }
    }
    public void StopX()
    {
        for (Element e : GetStars())
        {
            e.StopX();
        }
    }
    public void StopY()
    {
        for (Element e : GetStars())
        {
            e.StopY();
        }
    }


    public void GameUpdate()
    {
        for (Element e : GetStars())
        {
            e.GameUpdate();
        }
    }

    public void GameDraw(Graphics2D g)
    {
        for (Element e : GetStars())
        {
            e.GameDraw(g, "");
        }
    }
}
