package Engine_Game;

import java.io.*;
import java.lang.Math.*;
import java.util.*;
import Reusable.Functions;

public class Sector
{
    /* PROPERTIES */
    //
        /* CURRENT FILE AND ITS NEIGHBORING SECTOR FILES */
        //
            private String nwFile = "";
                public String GetNwFile() { return nwFile; }
                public void SetNwFile(String val) { nwFile = val; }

            private String nFile = "";
                public String GetNFile() { return nFile; }
                public void SetNFile(String val) { nFile = val; }

            private String neFile = "";
                public String GetNeFile() { return neFile; }
                public void SetNeFile(String val) { neFile = val; }

            private String wFile = "";
                public String GetWFile() { return wFile; }
                public void SetWFile(String val) { wFile = val; }

            private String cFile = "";
                public String GetCFile() { return cFile; }
                public void SetCFile(String val) { cFile = val; }

            private String eFile = "";
                public String GetEFile() { return eFile; }
                public void SetEFile(String val) { eFile = val; }

            private String swFile = "";
                public String GetSwFile() { return swFile; }
                public void SetSwFile(String val) { swFile = val; }

            private String sFile = "";
                public String GetSFile() { return sFile; }
                public void SetSFile(String val) { sFile = val; }

            private String seFile = "";
                public String GetSeFile() { return seFile; }
                public void SetSeFile(String val) { seFile = val; }
        //

        private int shiftX = 0;
            public int GetShiftX() { return shiftX; }
            public void SetShiftX(int val) { shiftX = val; }
            public void AddShiftX(int val) { shiftX += val; }

        private int shiftY = 0;
            public int GetShiftY() { return shiftY; }
            public void SetShiftY(int val) { shiftY = val; }
            public void AddShiftY(int val) { shiftY += val; }
    //


    public void SetupSectorFiles(String parentId, int x, int y)
    {
        SetupCurrent(parentId, x, y);
    }

    public int GetShiftX(int x)
    {
        int halfX = Game.GAMEWIDTH; // (Game.GAMEWIDTH / 2);
        int adjX = 0;
        int shift = 0;

        if (x > halfX)
        {
            shift = 1;
        }
        if (x < -halfX)
        {
            shift = -1;
        }

        return shift;
    }

    public int GetShiftY(int y)
    {
        int halfY = Game.GAMEHEIGHT; // (Game.GAMEHEIGHT / 2);
        int adjY = 0;
        int shift = 0;

        if (y > halfY)
        {
            shift = 1;
        }
        if (y < -halfY)
        {
            shift = -1;
        }

        return shift;
    }

    public void ParseFileShift(String sectorFile)
    {
        String[] arr = sectorFile.replace(".dat", "").split("_");

        if (arr.length == 3)
        {
            SetShiftX(Integer.parseInt(arr[1].trim()));
            SetShiftY(Integer.parseInt(arr[2].trim()));
        }
    }

    public String SetupCurrent(String parentId, int x, int y)
    {
        String output = "";
        String dir = "data/sectors/";
        int shiftX = GetShiftX(x);
        int shiftY = GetShiftY(y);

        if (shiftX != 0) AddShiftX(shiftX);
        if (shiftY != 0) AddShiftY(shiftY);

        output = parentId + "_" + GetShiftX() + "_" + GetShiftY() + ".dat";

        if (!GetCFile().equals(output))
        {
            SetCFile(output);
            SetupNeighbors(parentId, shiftX, shiftY);
        }

        return output;
    }

    public void SetupNeighbors(String parentId, int shiftX, int shiftY)
    {
        String filePath = "";

        // North West
        filePath = parentId + "_" + (GetShiftX() - 1) + "_" + (GetShiftY() - 1) + ".dat";
        SetNwFile(filePath);

        // North
        filePath = parentId + "_" + GetShiftX() + "_" + (GetShiftY() - 1) + ".dat";
        SetNFile(filePath);

        // North East
        filePath = parentId + "_" + (GetShiftX() + 1) + "_" + (GetShiftY() - 1) + ".dat";
        SetNeFile(filePath);

        // West
        filePath = parentId + "_" + (GetShiftX() - 1) + "_" + GetShiftY() + ".dat";
        SetWFile(filePath);

        // East
        filePath = parentId + "_" + (GetShiftX() + 1) + "_" + GetShiftY() + ".dat";
        SetEFile(filePath);

        // South West
        filePath = parentId + "_" + (GetShiftX() - 1) + "_" + (GetShiftY() + 1) + ".dat";
        SetSwFile(filePath);

        // South
        filePath = parentId + "_" + GetShiftX() + "_" + (GetShiftY() + 1) + ".dat";
        SetSFile(filePath);

        // South East
        filePath = parentId + "_" + (GetShiftX() + 1) + "_" + (GetShiftY() + 1) + ".dat";
        SetSeFile(filePath);
    }

    public boolean IsNewSector(String secFile)
    {
        String fil = "data/sectors/" + secFile;
        File f = new File(fil);

        return f.exists();
    }


    public String GetSectorGridFile(int gridPosX, int gridPosY)
    {
        String output = "";

        // W
        if (gridPosX == -1)
        {
            if (gridPosY == -1) output = GetNwFile();
            if (gridPosY == 0) output = GetWFile();
            if (gridPosY == 1) output = GetSwFile();
        }

        // Center
        if (gridPosX == 0)
        {
            if (gridPosY == -1) output = GetNFile();
            if (gridPosY == 0) output = GetCFile();
            if (gridPosY == 1) output = GetSFile();
        }

        // E
        if (gridPosX == 1)
        {
            if (gridPosY == -1) output = GetNeFile();
            if (gridPosY == 0) output = GetEFile();
            if (gridPosY == 1) output = GetSeFile();
        }

        return output;
    }

    public int GetSectorGridAdjustX(int gridPosX)
    {
        int output = 0;

        if (gridPosX == -1) output = -(Game.GAMEWIDTH);
        if (gridPosX == 1) output = Game.GAMEWIDTH;

        return output;
    }
    public int GetSectorGridAdjustY(int gridPosY)
    {
        int output = 0;

        if (gridPosY == -1) output = -(Game.GAMEHEIGHT);
        if (gridPosY == 1) output = Game.GAMEHEIGHT;

        return output;
    }
}
