package Engine_Game;

import java.io.*;
import java.util.*;
import java.awt.Graphics2D;
import Reusable.Functions;

public class Screen
{
    /* PROPERTIES */
    //
        private int screenType = 1;
            public int GetScreenType() { return screenType; }
            public void SetScreenType(int val) { screenType = val; }

        private Menu menu = null;
            public Menu GetMenu() { return menu; }
            public void SetMenu(Menu val) { menu = val; }

        private Level level = null;
            public Level GetLevel() { return level; }
            public void SetLevel(Level val) { level = val; }

        private int levelType = Level.LVL_SPACE;
            public int GetLevelType() { return levelType; }
            public void SetLevelType(int val) { levelType = val; }

        private Element screenImage = null;
            public Element GetScreenImage() { return screenImage; }
            public void SetScreenImage(Element val) { screenImage = val; }
    //


    public Screen(int screenTypeIn, int levelTypeIn)
    {
        SetScreenType(screenTypeIn);
        SetLevelType(levelTypeIn);

        SetupClasses();
    }

    private void SetupClasses()
    {
        Element e = null;
        String imgPath = "";
        String dir = "";

        if (GetScreenType() == Game.SCR_TITLE)
        {
            imgPath = "library/system/title.png";
            if (GetLevelType() == Level.LVL_IMAGE)
            {
                e = new Element("", Element.ELEM_IMAGE, "", "", 0, 0, Game.GAMEWIDTH, Game.GAMEHEIGHT, false);
                e.SetCenterElement(true);
                e.GetMove().SetSpeedX(0);
                e.GetMove().SetSpeedY(0);
                e.SetImagePath(imgPath);
                e.SetImage(e.GetImagePath());
                SetScreenImage(e);
            }
        }

        if (GetScreenType() == Game.SCR_PAUSE)
        {
            imgPath = "library/system/paused.png";
            if (GetLevelType() == Level.LVL_IMAGE)
            {
                e = new Element("", Element.ELEM_IMAGE, "", "", 0, 0, Game.GAMEWIDTH, Game.GAMEHEIGHT, false);
                e.SetCenterElement(true);
                e.GetMove().SetSpeedX(0);
                e.GetMove().SetSpeedY(0);
                e.SetImagePath(imgPath);
                e.SetImage(e.GetImagePath());
                SetScreenImage(e);
            }
        }

        if (GetScreenType() == Game.SCR_GAMEOVER)
        {
            imgPath = "library/system/game_over.png";
            if (GetLevelType() == Level.LVL_IMAGE)
            {
                e = new Element("", Element.ELEM_IMAGE, "", "", 0, 0, Game.GAMEWIDTH, Game.GAMEHEIGHT, false);
                e.SetCenterElement(true);
                e.GetMove().SetSpeedX(0);
                e.GetMove().SetSpeedY(0);
                e.SetImagePath(imgPath);
                e.SetImage(e.GetImagePath());
                SetScreenImage(e);
            }
        }

        if (GetScreenType() == Game.SCR_PLAY)
        {
            SetupLevel();
        }
    }

    public void SetupLevel()
    {
        String fil = "data/game.dat";
        String lvlDir = "data/levels";
        String lvlId = Reusable.Functions.GetSetting(fil, "level", "").trim();
        String parentId = "";
        String meta = "space";

        if (lvlId.equals(""))
        {
            lvlId = Functions.GetGUID();
            Functions.SetSetting(fil, "level", lvlId);
        }

        String lvlConfig = lvlDir + "/" + lvlId + ".dat";

        File f = new File(lvlConfig);
        int lvlType = GetLevelType();
        String dir = "";

        if (f.exists())
        {
            lvlType = Functions.GetSetting(lvlDir + "/" + lvlId + ".dat", "leveltype", GetLevelType());
            dir = Functions.GetSetting(lvlConfig, "libdir", "library/levels/space");
            parentId = Functions.GetSetting(lvlConfig, "parent", "");
        }
        else
        {
            dir = Functions.GetSetting(fil, "current_directory", "library/levels/space");
        }

        Level l = new Level(lvlId, lvlType, dir, parentId, meta);
        l.GetMove().SetReverse(true);
        SetLevel(l);
    }

    public void DoLevelTravel()
    {
        String trvFile = "";
        String newDir = GetLevel().GetTravelToDir();
        String hitId = "";
        String pDat = "";
        String id = "";
        String pId = "";
        int lvlType = 0;
        String lvlConfig = "";
        Move m = GetLevel().GetMove();
        Coord c = m.GetCoord();
        int left = 0;
        int top = 0;
        int posX = 0;
        int posY = 0;
        String meta = "";

        hitId = GetLevel().GetHitId();
        trvFile = "data/travel/" + hitId + ".dat";

        if (newDir.equals("{parent}"))
        {
            lvlConfig = "data/levels/" + GetLevel().GetId() + ".dat";
            pId = Functions.GetSetting(lvlConfig, "parent", "");
            lvlConfig = "data/levels/" + pId + ".dat";
            newDir = Functions.GetSetting(lvlConfig, "libdir", "");
            id = pId;
            pDat = lvlConfig;
            lvlType = Functions.GetSetting(lvlConfig, "leveltype", 1);
            pId = Functions.GetSetting(lvlConfig, "parent", "");

            left = Functions.GetSetting(lvlConfig, "left", 0);
            top = Functions.GetSetting(lvlConfig, "top", 0);
            posX = Functions.GetSetting(lvlConfig, "positionx", 0);
            posY = Functions.GetSetting(lvlConfig, "positiony", 0);
            meta = Functions.GetSetting(lvlConfig, "meta", "");
        }
        else
        {
            lvlConfig = "data/levels/" + GetLevel().GetId() + ".dat";

            trvFile = "data/travel/" + hitId + ".dat";
            File f = new File(trvFile);

            if (f.exists())
            {
                newDir = Functions.GetSetting(trvFile, "target_dir", "").trim();
                try
                {
                    lvlType = Integer.parseInt(Functions.GetSetting(trvFile, "target_type", Level.LVL_SUB + "").trim());
                }
                catch (Exception ex)
                {
                    lvlType = Level.LVL_SUB;
                }
                id = Functions.GetSetting(trvFile, "target_id", Functions.GetGUID()).trim();
                pId = Functions.GetSetting(trvFile, "parent_id", "").trim();
                meta = Functions.GetSetting(trvFile, "meta", "").trim();
            }
            else
            {
                try
                {
                    f.createNewFile();
                }
                catch (Exception ex)
                {}

                lvlType = Level.LVL_SUB;
                List<String> dirs = Functions.ListDirs(newDir, "player");
                int ix = Functions.RandomInt(0, dirs.size());
                newDir = newDir + "/" + dirs.get(ix);
                String settings = newDir + "/settings.config";
                pId = GetLevel().GetId();
                id = Functions.GetSetting(settings, "id", Functions.GetGUID());
                meta = Functions.GetSetting(settings, "meta", "");

                Functions.SetSetting(trvFile, "target_dir", newDir);
                Functions.SetSetting(trvFile, "target_type", lvlType + "");
                Functions.SetSetting(trvFile, "target_id", id);
                Functions.SetSetting(trvFile, "parent_id", pId);
                Functions.SetSetting(trvFile, "meta", meta);
            }
        }

        Level l = new Level(id, lvlType, newDir, pId, meta);
        l.GetMove().SetReverse(true);

        m = l.GetMove();
        c = m.GetCoord();

        c.SetLeft(left);
        c.SetTop(top);
        c.SetPositionX(posX);
        c.SetPositionY(posY);

        SetLevel(l);
    }

    public void DoMenuShow()
    {
        int mType = Functions.ToInt(GetLevel().GetMenuType());
        if (!Menu.ValidMenuType(mType)) return;

        Menu m = new Menu(mType);
        SetMenu(m);
    }


    public void SaveGame()
    {
        Level l = GetLevel();
        if (l == null) return;

        l.SaveGame();
    }



    // STANDARD GAME METHODS
    public void KeyPressed(int keyCode)
    {
        boolean handled = false;

        if (GetMenu() != null)
        {
            handled = true;
            GetMenu().KeyPressed(keyCode);
        }

        if (!handled && GetLevel() != null)
        {
            GetLevel().KeyPressed(keyCode);
        }
    }

    public void KeyReleased(int keyCode)
    {
        boolean handled = false;

        if (GetMenu() != null)
        {
            handled = true;
            GetMenu().KeyReleased(keyCode);
        }

        if (!handled && GetLevel() != null)
        {
            GetLevel().KeyReleased(keyCode);
        }

    }

    public void GameUpdate()
    {
        boolean handled = false;

        if (GetMenu() != null)
        {
            handled = true;
            GetMenu().GameUpdate();
            // if (!GetLevel().GetTravelToDir().equals("")) DoLevelTravel();
        }

        if (!handled && GetLevel() != null)
        {
            GetLevel().GameUpdate();
            if (!GetLevel().GetTravelToDir().equals("")) DoLevelTravel();
            if (!GetLevel().GetMenuType().equals("")) DoMenuShow();
        }
    }

    public void GameDraw(Graphics2D g)
    {
        if (GetLevel() != null)
        {
            GetLevel().GameDraw(g);
        }

        if (GetScreenImage() != null)
        {
            GetScreenImage().GameDraw(g, "");
        }

        if (GetMenu() != null)
        {
            GetMenu().GameDraw(g);
        }
    }
}
