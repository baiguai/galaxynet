package Engine_Game;

import java.util.*;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.Color;
import Reusable.Functions;

public class Stats
{
    /* PROPERTIES */
    //
        private int rowHeight = 14;
            public int GetRowHeight() { return rowHeight; }

        private int padding = 5;
            public int GetPadding() { return padding; }

        private int columnWidth = 300;
            public int GetColumnWidth() { return columnWidth; }

        private Color color;
            public Color GetColor() { return color; }
            public void SetColor(Color val) { color = val; }

        private int life = 0;
            private int newLife = 0;
            public int GetLife() { return life; }
            public void SetLife(int val) { life = val; }
            public void AddLife(int val) { newLife = life + val; }
            public void IncrementLife() { if (newLife > life) life = life + 1; else newLife = 0; }
            public void ResetLife() { life = 0; }

        private int score = 0;
            private int newScore = 0;
            public int GetScore() { return score; }
            public void SetScore(int val) { score = val; }
            public void AddScore(int val) { newScore = score + val; }
            public void IncrementScore() { if (newScore > score) score = score + 1; else newScore = 0; }
            public void ResetScore() { score = 0; }

        private int level = 1;
            public int GetLevel() { return level; }
            public void SetLevel(int val) { level = val; }
            public void AddLevel(int val) { level = level + val; }
            public void ResetLevel() { level = 1; }

        private boolean useFuel = false;
            public boolean GetUseFuel() { return useFuel; }
            public void SetUseFuel(boolean val) { useFuel = val; }

        private int fuelBurnRate = 50;
            public int GetFuelBurnRate() { return fuelBurnRate; }
            public void SetFuelBurnRate(int val) { fuelBurnRate = val; }

        private int fuel = 100;
            private int newFuel = 0;
            public int GetFuel() { return fuel; }
            public void SetFuel(int val) { if (val < 0) val = 0; fuel = val; }
            public void SubtractFuel(int val) { if (newFuel > fuel) { fuel = newFuel; newFuel = 0; } fuel = fuel - val; if (fuel < 0) fuel = 0; }
            public void BurnFuel() { SubtractFuel(GetFuelBurnRate()); }
            public void AddFuel(int val) { newFuel = GetFuel() + val; }
            public void IncrementFuel() { if (newFuel > fuel) fuel = fuel + 1; else newFuel = 0; }
            public void ResetFuel() { fuel = 100; }

        private int minerals = 0;
            private int newMinerals = 0;
            public int GetMinerals() { return minerals; }
            public void SetMinerals(int val) { if (val < 0) val = 0; minerals = val; }
            public void SubtractMinerals(int val) { minerals = minerals - val; if (minerals < 0) minerals = 0; }
            public void AddMinerals(int val) { newMinerals = minerals + val; }
            public void IncrementMinerals() { if (newMinerals > minerals) minerals = minerals + 1; else newMinerals = 0; }
            public void ResetMinerals() { minerals = 0; }

        private int positionX = 0;
            public int GetPositionX() { return positionX; }
            public void SetPositionX(int val) { positionX = val; }

        private int positionY = 0;
            public int GetPositionY() { return positionY; }
            public void SetPositionY(int val) { positionY = val; }

        private boolean usePosition = false;
            public boolean GetUsePosition() { return usePosition; }
            public void SetUsePosition(boolean val) { usePosition = val; }
    //


    public Stats(Color colorIn, boolean useFuelIn, boolean usePositionIn)
    {
        String path = "data/game.dat";
        SetColor(colorIn);
        SetUseFuel(useFuelIn);
        SetUsePosition(usePositionIn);

        SetFuelBurnRate(Functions.GetSetting(path, "fuel_burn_rate", 50));
        SetLevel(Functions.GetSetting(path, "curlevel", 1));
        SetLife(Functions.GetSetting(path, "life", 100));
        SetScore(Functions.GetSetting(path, "score", 0));
        SetMinerals(Functions.GetSetting(path, "minerals", 0));
        SetFuel(Functions.GetSetting(path, "fuel", 100));
    }


    public void FixStats()
    {
        if (newLife > GetLife()) SetLife(newLife);
        if (newScore > GetScore()) SetScore(newScore);
        if (newFuel > GetFuel()) SetFuel(newFuel);
        if (newMinerals > GetMinerals()) SetMinerals(newMinerals);
    }


    public void GameUpdate(int positionXIn, int positionYIn)
    {
        if (newLife > GetLife())
        {
            IncrementLife();
        }

        if (newScore > GetScore())
        {
            IncrementScore();
        }

        if (newFuel > GetFuel())
        {
            IncrementFuel();
        }

        if (newMinerals > GetMinerals())
        {
            IncrementMinerals();
        }


        SetPositionX(positionXIn);
        SetPositionY(positionYIn);
    }

    public void GameDraw(Graphics2D g)
    {
        int y = GetRowHeight() * 2;

        g.setColor(GetColor());
        g.setFont(new Font("Century Gothic", Font.PLAIN, GetRowHeight()));

        g.drawString("Life:  " + GetLife(), Game.GAMEWIDTH - GetColumnWidth(), y);

        y += GetRowHeight() + GetPadding();
        g.drawString("Score:  " + GetScore(), Game.GAMEWIDTH - GetColumnWidth(), y);

        y += GetRowHeight() + GetPadding();
        g.drawString("Level:  " + GetLevel(), Game.GAMEWIDTH - GetColumnWidth(), y);

        if (GetUseFuel())
        {
            y += GetRowHeight() + GetPadding();
            g.drawString("Fuel:  " + GetFuel(), Game.GAMEWIDTH - GetColumnWidth(), y);
        }

        y += GetRowHeight() + GetPadding();
        g.drawString("Minerals:  " + GetMinerals(), Game.GAMEWIDTH - GetColumnWidth(), y);

        if (GetUsePosition())
        {
            y += GetRowHeight() + GetPadding();
            g.drawString("Location:  " + GetPositionX() + ", " + GetPositionY(), Game.GAMEWIDTH - GetColumnWidth(), y);
        }

        g.setColor(Color.BLACK);
    }
}
