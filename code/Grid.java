package Engine_Game;

import java.io.*;
import java.util.*;
import java.awt.Color;
import java.awt.Graphics2D;
import Reusable.Functions;

public class Grid
{
    /* properties */
    //
        private Color gridColor = new Color(0,0,0,255);
            public Color GetGridColor() { return gridColor; }
            public void SetGridColor(Color val) { gridColor = val; }

        private int gridSpacing = 32;
            public int GetGridSpacing() { return gridSpacing; }
            public void SetGridSpacing(int val) { gridSpacing = val; }

        private int offsetX = 0;
            public int GetOffsetX() { return offsetX; }
            public void SetOffsetX(int val) { offsetX = val; }
            public void AddOffsetX(int val) { offsetX += val; }

        private int offsetY = 0;
            public int GetOffsetY() { return offsetY; }
            public void SetOffsetY(int val) { offsetY = val; }
            public void AddOffsetY(int val) { offsetY += val; }

        private List<Integer> gridX = null;
            public List<Integer> GetGridX() { if (gridX == null) gridX = new ArrayList<Integer>(); return gridX; }
            public void SetGridX(List<Integer> val) { gridX = val; }
            public void AddGridX(int val) { GetGridX().add(val); }
            public void ClearGridX() { gridX = null; }

        private List<Integer> gridY = null;
            public List<Integer> GetGridY() { if (gridY == null) gridY = new ArrayList<Integer>(); return gridY; }
            public void SetGridY(List<Integer> val) { gridY = val; }
            public void AddGridY(int val) { GetGridY().add(val); }
            public void ClearGridY() { gridY = null; }
    //


    public Grid(Color gridColorIn, int gridSpacingIn)
    {
        SetGridColor(gridColorIn);
        SetGridSpacing(gridSpacingIn);

        SetupClasses();
    }

    public void SetupClasses()
    {
        int pos = 0;

        pos = GetOffsetX();
        ClearGridX();
        for (int x = pos; x <= Game.GAMEWIDTH + GetGridSpacing(); x += GetGridSpacing())
        {
            AddGridX(x);
        }

        pos = GetOffsetY();
        ClearGridY();
        for (int y = pos; y <= Game.GAMEHEIGHT + GetGridSpacing(); y += GetGridSpacing())
        {
            AddGridY(y);
        }
    }

    public void RemoveFirst(boolean xAxis)
    {
        if (xAxis)
        {
            if (GetOffsetX() >= -(GetGridSpacing())) return;
            SetOffsetX(GetOffsetX() + GetGridSpacing());
        }
        else
        {
            if (GetOffsetY() >= -(GetGridSpacing())) return;
            SetOffsetY(GetOffsetY() + GetGridSpacing());
        }
    }

    public void RemoveLast(boolean xAxis)
    {
        if (xAxis)
        {
            if ((GetGridX().get(GetGridX().size()-1) + GetOffsetX()) <= (Game.GAMEWIDTH + GetGridSpacing())) return;
            SetOffsetX(GetOffsetX() - GetGridSpacing());
        }
        else
        {
            if ((GetGridY().get(GetGridY().size()-1) + GetOffsetY()) <= (Game.GAMEHEIGHT + GetGridSpacing())) return;
            SetOffsetY(GetOffsetY() - GetGridSpacing());
        }
    }


    public void GameDraw(Graphics2D g)
    {
        g.setColor(GetGridColor());

        for (int i : GetGridX())
        {
            g.drawLine(i + GetOffsetX(), 0, i + GetOffsetX(), Game.GAMEHEIGHT);
        }

        for (int i : GetGridX())
        {
            g.drawLine(0, i + GetOffsetY(), Game.GAMEWIDTH, i + GetOffsetY());
        }

        g.setColor(new Color(0, 0, 0, 255));
    }
}
