package Engine_Kernel;

import javax.swing.*;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Point;
import java.awt.Toolkit;
import java.awt.Graphics2D;
import java.awt.Graphics;
import java.awt.Dimension;
import java.awt.RenderingHints;
import java.awt.image.*;
import java.awt.event.*;
import java.util.*;
import java.io.*;
import java.io.IOException;
import javax.imageio.ImageIO;
import java.net.URL;

/*
    Game Loop
    ----------------------------------------------------------------------------
        The Game Panel houses the game loop logic and calls the core game
        methods and houses the Game object which runs the game.
    ----------------------------------------------------------------------------
*/

public class GamePanel extends JPanel implements Runnable, KeyListener, MouseListener
{
    /* PROPERTIES */
    //
        private Thread thread;
            public Thread GetThread() { return thread; }
            public void SetThread(Thread val) { thread = val; }

        private boolean running;
            public boolean GetRunning() { return running; }
            public void SetRunning(boolean val) { running = val; }

        private BufferedImage buffImage;
            public BufferedImage GetBuffImage() { return buffImage; }
            public void SetBuffImage(BufferedImage val) { buffImage = val; }

        private Graphics2D graphics;
            public Graphics2D GetGraphics() { return graphics; }
            public void SetGraphics(Graphics2D val) { graphics = val; }

        private int fps = 30;
            public int GetFPS() { return fps; }

        private int width = 0;
            public int GetWidth() { return width; }
            public void SetWidth(int val) { width = val; }

        private int height = 0;
            public int GetHeight() { return height; }
            public void SetHeight(int val) { height = val; }

        private Engine_Game.Game game = null;
            public Engine_Game.Game GetGame() { return game; }
            public void SetGame(Engine_Game.Game val) { game = val; }
    //


    public GamePanel(int widthIn, int heightIn)
    {
        super();

        HideCursor();

        SetWidth(widthIn);
        SetHeight(heightIn);

        // SetCurScreen(Global.Screen_Start);

        // SETUP THE 2D GRAPHICS
        SetBuffImage(new BufferedImage(GetWidth(), GetHeight(), BufferedImage.TYPE_INT_ARGB));
        SetGraphics((Graphics2D) GetBuffImage().getGraphics());
        GetGraphics().setRenderingHint(
            RenderingHints.KEY_ANTIALIASING,
            RenderingHints.VALUE_ANTIALIAS_ON);
        GetGraphics().setRenderingHint(
            RenderingHints.KEY_TEXT_ANTIALIASING,
            RenderingHints.VALUE_TEXT_ANTIALIAS_ON);

        GetGraphics().setColor(new Color(0, 0, 0, 255));
        GetGraphics().fillRect(0, 0, GetWidth(), GetHeight());

        SetupClasses();
    }

    /*
        Initializes the screen classes and the initial level class.
    */
    private void SetupClasses()
    {
        Engine_Game.Game g = new Engine_Game.Game(GetWidth(), GetHeight());
        SetGame(g);
    }

    /*
        Sets up the game loop and starts it running.
    */
    public void run()
    {
        SetRunning(true);

        long startTime;
        long URDTimeMillis;
        long waitTime;
        long totalTime = 0;
        int frameCount = 0;
        int maxFrameCount = 30;
        long targetTime = 1000 / GetFPS();

        while (running)
        {
            startTime = System.nanoTime();

            // Primary methods per loop
            GameUpdate();
            GameDraw();
            GameRender();

            // FPS Throttling
            URDTimeMillis = (System.nanoTime() - startTime) / 1000000;
            waitTime = targetTime - URDTimeMillis;
            try
            {
                Thread.sleep(waitTime);
            }
            catch(Exception ex) { }

            totalTime += System.nanoTime() - startTime;
            frameCount++;

            if (frameCount == maxFrameCount)
            {
                // SetAverageFPS(1000.0 / ((totalTime / frameCount) / 1000000));
                URDTimeMillis = 0;
                waitTime = 0;
                startTime = System.nanoTime();
                frameCount = 0;
                totalTime = 0;
            }
        }
    }

    public void addNotify() 
    {
        // The inherited addNotify method is called
        super.addNotify();

        // Create the game thread
        if (GetThread() == null)
        {
            SetThread(new Thread(this));
            GetThread().start();
        }

        // Create the key listener
        this.setFocusable(true);
        this.requestFocus();
        addKeyListener(this);
        addMouseListener(this);
    }


    public void mouseMoved(MouseEvent e)
    {
        e = null;
    }
    public void mouseMoving(MouseEvent e)
    {
        e = null;
    }
    public void mouseEntered(MouseEvent e)
    {
        e = null;
    }
    public void mouseExited(MouseEvent e)
    {
        e = null;
    }
    public void mousePressed(MouseEvent e)
    {
        e = null;
    }
    public void mouseReleased(MouseEvent e)
    {
        e = null;
    }
    public void mouseDragged(MouseEvent e)
    {
        e = null;
    }
    public void mouseClicked(MouseEvent e) {
        Point p = e.getPoint();
    }
    public void keyTyped(KeyEvent key)
    {
        key = null;
    }
    public void keyPressed(KeyEvent key)
    {
        int keyCode = key.getKeyCode();

        GetGame().KeyPressed(keyCode);

        // call the Game's keyPressed method
        /*
            boolean collided = false;
            boolean fading = false;

            if (GetFade() != null && GetFade().GetFading()) fading = true;

            if (keyCode == KeyEvent.VK_SPACE)
            {
            }

            if (GetCurScreen() == SCR_PLAY && !fading && !spawning)
            {
                // Movement
                CollisionDetail test = null;
                test = GetPlayer().GetCollision().CollisionCheck();

                if (test != null && (
                    test.GetCollidedWith().GetCollision().GetType() == Collision.COL_OBJECT
                    ||
                    test.GetCollidedWith().GetCollision().GetType() == Collision.COL_ADJUST
                ))
                {
                    if (!GetScrSafety()) collided = true;
                }

                if (keyCode == KeyEvent.VK_LEFT)
                {
                    if (collided && test.GetCollidedX() > 0)
                    {
                        GetLevel().StopX();
                        GetLevel().StopY();
                        key = null;
                        return;
                    }
                    HideCursor();
                    GetPlayer().SetMovingX(-1);
                    GetLevel().SetMovingX(-1);
                }
                if (keyCode == KeyEvent.VK_RIGHT)
                {
                    if (collided && test.GetCollidedX() < 0)
                    {
                        GetLevel().StopX();
                        GetLevel().StopY();
                        key = null;
                        return;
                    }
                    HideCursor();
                    GetPlayer().SetMovingX(1);
                    GetLevel().SetMovingX(1);
                }
                if (keyCode == KeyEvent.VK_UP)
                {
                    if (collided && test.GetCollidedY() > 0)
                    {
                        GetLevel().StopX();
                        GetLevel().StopY();
                        key = null;
                        return;
                    }
                    HideCursor();
                    GetPlayer().SetMovingY(-1);
                    GetLevel().SetMovingY(-1);
                }
                if (keyCode == KeyEvent.VK_DOWN)
                {
                    if (collided && test.GetCollidedY() < 0)
                    {
                        GetLevel().StopX();
                        GetLevel().StopY();
                        key = null;
                        return;
                    }
                    HideCursor();
                    GetPlayer().SetMovingY(1);
                    GetLevel().SetMovingY(1);
                }
            }
        */

        key = null;
    }
    public void keyReleased(KeyEvent key) 
    {
        int keyCode = key.getKeyCode();

        GetGame().KeyReleased(keyCode);

        // call the Game's keyReleased method
        /*
            boolean movingX = true;
            boolean movingY = true;
            boolean handled = false;
            boolean fading = false;

            if (GetFade() != null && GetFade().GetFading()) fading = true;


            if (keyCode == KeyEvent.VK_ESCAPE)
            {
                if (GetCurScreen() == SCR_UPGRADE)
                {
                    SetCurScreen(SCR_PLAY);
                    key = null;
                    return;
                }
            }

            if (keyCode == KeyEvent.VK_P)
            {
                handled = false;

                if (!handled && GetCurScreen() == SCR_TITLE)
                {
                    SetCurScreen(SCR_PLAY);
                    handled = true;
                }

                if (!handled && GetCurScreen() == SCR_PLAY && !fading)
                {
                    SetCurScreen(SCR_PAUSE);
                    handled = true;
                }

                if (!handled && GetCurScreen() == SCR_PAUSE)
                {
                    SetCurScreen(SCR_PLAY);
                    handled = true;
                }
            }

            if (keyCode == KeyEvent.VK_R)
            {
                if (!handled && GetCurScreen() == SCR_TITLE)
                {
                    String gameData = "data/game";
                    String levelData = "data/levels/";

                    Reusable.Functions.DeleteFile(gameData);
                    Reusable.Functions.ClearFolder(levelData, "");

                    Game.CreateFolders();

                    SetCurScreen(SCR_PLAY);
                }
            }

            if (keyCode == KeyEvent.VK_Q)
            {
                if (GetLevel().GetObjectsRemoved())
                {
                    GetLevel().SaveRemoval();
                }
                else
                {
                    GetLevel().SavePosition();
                }
                System.exit(0);
            }

            if (keyCode == KeyEvent.VK_N)
            {
            }

            if (GetCurScreen() == SCR_PLAY && !fading)
            {
                if (keyCode == KeyEvent.VK_LEFT ||
                    keyCode == KeyEvent.VK_RIGHT)
                {
                    movingX = false;
                    GetLevel().SetMovingX(0);
                }
                if (keyCode == KeyEvent.VK_UP ||
                    keyCode == KeyEvent.VK_DOWN)
                {
                    movingY = false;
                    GetLevel().SetMovingY(0);
                }

                if (keyCode == KeyEvent.VK_SPACE && GetLevel().GetType() == Level.LVL_SURFACE)
                {
                    ShootMissile();
                }
            }

            if (GetCurScreen() == SCR_UPGRADE)
            {
                if (keyCode == KeyEvent.VK_LEFT)
                {
                    GetUpgradeScreen().SetMovingX(-1);
                }
                if (keyCode == KeyEvent.VK_RIGHT)
                {
                    GetUpgradeScreen().SetMovingX(1);
                }
                if (keyCode == KeyEvent.VK_UP)
                {
                    GetUpgradeScreen().SetMovingY(-1);
                }
                if (keyCode == KeyEvent.VK_DOWN)
                {
                    GetUpgradeScreen().SetMovingY(1);
                }

                if (keyCode == KeyEvent.VK_ENTER)
                {
                    Element upg = GetUpgradeScreen().PickUpgrade();
                    if (upg != null)
                    {
                        GetLevel().GetStats().SetMinerals(GetLevel().GetStats().GetMinerals() - upg.GetCost());
                    }

                    // GetPlayer().AddUpgrade(upg);
                    SetCurScreen(SCR_PLAY);
                }
            }

            // if (!movingX || !movingY) ShowCursor();
        */

        key = null;
    }



    public void HideCursor()
    {
        BufferedImage cursorImg = new BufferedImage(16, 16, BufferedImage.TYPE_INT_ARGB);
        Cursor blankCursor = Toolkit.getDefaultToolkit().createCustomCursor(cursorImg, new Point(0, 0), "blank cursor");
        this.setCursor(blankCursor);
    }
    public void ShowCursor()
    {
        this.setCursor(Cursor.getPredefinedCursor(Cursor.CROSSHAIR_CURSOR));
    }



    // CORE GAME METHODS
    private void GameUpdate()
    {
        GetGame().GameUpdate();
    }

    private void GameDraw()
    {
        GetGraphics().setColor(new Color(0, 0, 0, 255));
        GetGraphics().fillRect(0, 0, GetWidth(), GetHeight());

        GetGame().GameDraw(GetGraphics());
    }

    private void GameRender()
    {
        Graphics g2 = this.getGraphics();
        g2.drawImage(GetBuffImage(), 0, 0, null);
        g2.dispose();
    }
}
