package Engine_Game;

public class CollisionDetail
{
    /* PROPERTIES */
    //
        private Element collidedWith = null;
            public Element GetCollidedWith() { return collidedWith; }
            public void SetCollidedWith(Element val) { collidedWith = val; }

        private int collidedX = 0;
            public int GetCollidedX() { return collidedX; }
            public void SetCollidedX(int val) { collidedX = val; }

        private int collidedY = 0;
            public int GetCollidedY() { return collidedY; }
            public void SetCollidedY(int val) { collidedY = val; }
    //
}
