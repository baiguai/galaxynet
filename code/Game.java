package Engine_Game;

import java.io.*;
import java.util.*;
import java.awt.event.*;
import java.awt.Graphics2D;
import Reusable.Functions;

/*

    Notes:
    The Game class is a necessary evil.
    Keep the amount of 'things' here to a minimum.
    It allows us to have global access to some necessary pieces of data
    without having to pass an instance around.

*/

public class Game
{
    /* PROPERTIES */
    //
        /* ENUMS */
        //
            public static int SCR_TITLE = 1;
            public static int SCR_PLAY = 10;
            public static int SCR_PAUSE = 20;
            public static int SCR_GAMEOVER = 30;
        //

        public static int GAMEWIDTH;
        public static int GAMEHEIGHT;

        private int curScreen = 10;
            public int GetCurScreen() { return curScreen; }
            public void SetCurScreen(int val) { curScreen = val; }

        private List<Screen> screens = null;
            public List<Screen> GetScreens() { if (screens == null) screens = new ArrayList<Screen>(); return screens; }
            public Screen GetScreen(int val)
            {
                Screen output = null;

                for (Screen s : GetScreens())
                {
                    if (s.GetScreenType() == val)
                    {
                        output = s;
                        break;
                    }
                }

                if (output == null) output = GetScreen(SCR_TITLE);

                return output;
            }
            public Screen GetCurrentScreen() { return GetScreen(GetCurScreen()); }
            public void SetScreens(List<Screen> val) { screens = val; }
            public void ClearScreens() { screens = null; }
            public void AddScreen(Screen val) { GetScreens().add(val); }
    //


    public Game(int widthIn, int heightIn)
    {
        GAMEWIDTH = widthIn;
        GAMEHEIGHT = heightIn;
        CreateFolders();

        SetCurScreen(SCR_TITLE);
        SetupClasses();
    }

    private void SetupClasses()
    {
        Screen s = null;

        ClearScreens();

        s = new Screen(SCR_TITLE, Level.LVL_IMAGE);
        AddScreen(s);

        s = new Screen(SCR_PLAY, Level.LVL_SPACE);
        AddScreen(s);

        s = new Screen(SCR_PAUSE, Level.LVL_IMAGE);
        AddScreen(s);

        s = new Screen(SCR_GAMEOVER, Level.LVL_IMAGE);
        AddScreen(s);
    }


    public void CreateFolders()
    {
        /*
            File gameDataDir = new File("data");
            File dir = null;
            String file = "";
            List<String> fileData = new ArrayList<String>();

            if (!gameDataDir.exists())
            {
                gameDataDir.mkdir();
            }

            String gameData = "data/game";
            File gd = new File(gameData);
            if (!gd.exists())
            {
                int fuelMin = 300;
                int fuelMax = 800;
                int fuel = Reusable.Functions.RandomInt(fuelMin, fuelMax);
                String level1 = "config/levels/1";

                fileData.add("curlevel=1");
                fileData.add("curlevelid=0");
                fileData.add("score=0");
                fileData.add("minerals=0");
                fileData.add("fuel=" + fuel);
                Reusable.Functions.WriteToFile(gameData, fileData);

                File ld = new File(level1);
                if (!ld.exists())
                {
                    fileData = new ArrayList<String>();
                    fileData.add("levelsize=250000");
                    fileData.add("planetmin=10000");
                    fileData.add("planetmax=40000");
                    fileData.add("energymin=6000");
                    fileData.add("energymax=10000");
                    Reusable.Functions.WriteToFile(level1, fileData);
                }
            }

            gameDataDir = new File("data/levels");
            if (!gameDataDir.exists()) gameDataDir.mkdir();

            file = "data/player";
            dir = new File(file);
            if (!dir.exists()) dir.mkdir();

            fileData = new ArrayList<String>();

            file = "data/player/space";
            dir = new File(file);
            if (!dir.exists()) dir.mkdir();
            file += "/upgrades";
            dir = new File(file);
            if (!dir.exists()) Reusable.Functions.WriteToFile(file, fileData);

            file = "data/player/surface";
            dir = new File(file);
            if (!dir.exists()) dir.mkdir();
            file += "/upgrades";
            dir = new File(file);
            if (!dir.exists()) Reusable.Functions.WriteToFile(file, fileData);

            file = "data/player/base";
            dir = new File(file);
            if (!dir.exists()) dir.mkdir();
            file += "/upgrades";
            dir = new File(file);
            if (!dir.exists()) Reusable.Functions.WriteToFile(file, fileData);
        */
    }


    public void GameOverCheck()
    {
        boolean gameOver = false;
        if (GetCurScreen() != SCR_PLAY) return;

        Level l = GetCurrentScreen().GetLevel();
        if (l == null) return;
        Level_Sub sub = l.GetSubLevel();
        if (sub == null) return;
        Stats st = sub.GetStats();
        if (st == null) return;

        if (!gameOver && st.GetFuel() < 1) gameOver = true;

        if (gameOver)
        {
            SetCurScreen(SCR_GAMEOVER);
        }
    }



    // STANDARD GAME METHODS
    public void KeyPressed(int keyCode)
    {
        GetCurrentScreen().KeyPressed(keyCode);
    }

    public void KeyReleased(int keyCode)
    {
        if (keyCode == KeyEvent.VK_Q)
        {
            GetCurrentScreen().SaveGame();
            System.exit(0);
        }

        if (keyCode == KeyEvent.VK_P)
        {
            if (GetCurScreen() == SCR_TITLE || GetCurScreen() == SCR_PAUSE)
            {
                SetCurScreen(SCR_PLAY);
                return;
            }
            if (GetCurScreen() == SCR_PLAY)
            {
                SetCurScreen(SCR_PAUSE);
                return;
            }
        }


        GetCurrentScreen().KeyReleased(keyCode);
    }

    public void GameUpdate()
    {
        GetCurrentScreen().GameUpdate();
        GameOverCheck();
    }

    public void GameDraw(Graphics2D g)
    {
        GetCurrentScreen().GameDraw(g);
        if (GetCurScreen() == SCR_PAUSE) GameDrawPlaying(g);
    }

    public void GameDrawPlaying(Graphics2D g)
    {
        GetScreen(SCR_PLAY).GameDraw(g);
    }
}
