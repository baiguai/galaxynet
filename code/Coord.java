package Engine_Game;

public class Coord
{
    /* PROPERTIES */
    //
        private int left = 0;
            public int GetLeft() { return left; }
            public void SetLeft(int val) { left = val; }
            public void AddLeft(int val) { left += val; }

        private int top = 0;
            public int GetTop() { return top; }
            public void SetTop(int val) { top = val; }
            public void AddTop(int val) { top += val; }

        private int positionX = 0;
            public int GetPositionX() { return positionX; }
            public void SetPositionX(int val) { positionX = val; }
            public void AddPositionX(int val) { positionX += val; }

        private int positionY = 0;
            public int GetPositionY() { return positionY; }
            public void SetPositionY(int val) { positionY = val; }
            public void AddPositionY(int val) { positionY += val; }

        private int width = 10;
            public int GetWidth() { return width; }
            public void SetWidth(int val) { width = val; }

        private int height = 10;
            public int GetHeight() { return height; }
            public void SetHeight(int val) { height = val; }

        private int gameWidth = 10;
            public int GetGameWidth() { return gameWidth; }
            public void SetGameWidth(int val) { gameWidth = val; }

        private int gameHeight = 10;
            public int GetGameHeight() { return gameHeight; }
            public void SetGameHeight(int val) { gameHeight = val; }

        private int right;
            public int GetRight() { return (GetLeft() + GetWidth()); }

        private int bottom;
            public int GetBottom() { return (GetTop() + GetHeight()); }

        private int centerX;
            public int GetCenterX() { return (GetWidth() / 2); }

        private int centerY;
            public int GetCenterY() { return (GetHeight() / 2); }
    //


    public Coord(int widthIn, int heightIn, int gameWidthIn, int gameHeightIn)
    {
        SetWidth(widthIn);
        SetHeight(heightIn);
        SetGameWidth(gameWidthIn);
        SetGameHeight(gameHeightIn);
    }

    public boolean InView()
    {
        boolean output = true;

        if (
            GetLeft() < 0 ||
            GetLeft() > Game.GAMEWIDTH ||
            GetTop() < 0 ||
            GetTop() > Game.GAMEHEIGHT
        )
        {
            output = false;
        }

        return output;
    }
}
