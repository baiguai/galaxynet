package Engine_Game;

import java.io.*;
import java.util.*;
import java.awt.Color;
import java.awt.event.*;
import java.awt.Graphics2D;
import Reusable.Functions;

//  NOTES
//  ----------------------------------------------------------------------------
/*
    The Level class is the 'conductor' managing the sub level's elements etc.


    [ ] Position Issue
        When returning to a level's parent, the position isn't restoring where
        the player was before entering the child level.

*/


public class Level
{
    /* PROPERTIES */
    //
        /* ENUMS */
        //
            public static int LVL_SPACE = 1;
            public static int LVL_SUB = 10;
            public static int LVL_IMAGE = 20;
            public static int LVL_MENU = 30;
        //

        private boolean SECTORS_LOADED = false;

        private int levelType = 1;
            public int GetLevelType() { return levelType; }
            public void SetLevelType(int val) { levelType = val; }

        private String levelMeta = "";
            public String GetLevelMeta() { return levelMeta; }
            public void SetLevelMeta(String val) { levelMeta = val; }

        private String id = "";
            public String GetId() { return id; }
            public void SetId(String val) { id = val; }

        private String parentId = "";
            public String GetParentId() { return parentId; }
            public void SetParentId(String val) { parentId = val; }

        private String libraryDir = "";
            public String GetLibraryDir() { return libraryDir; }
            public void SetLibraryDir(String val) { libraryDir = val; }

        private DevStats devStats = null;
            public DevStats GetDevStats() { return devStats; }
            public void SetDevStats(DevStats val) { devStats = val; }

        private int playerSpeed = 4;
            public int GetPlayerSpeed() { return playerSpeed; }
            public void SetPlayerSpeed(int val) { playerSpeed = val; }

        private Move move = null;
            public Move GetMove() { return move; }
            public void SetMove(Move val) { move = val; }

        private String curSector = "";
            public String GetCurSector() { return curSector; }
            public void SetCurSector(String val) { curSector = val; }

        private Sector sectors = null;
            public Sector GetSectors() { if (sectors == null) sectors = new Sector(); return sectors; }
            public void SetSectors(Sector val) { sectors = val; }
            public void ClearSectors() { sectors = null; }

        private Color background = new Color(0,0,0,255);
            public Color GetBackground() { return background; }
            public void SetBackground(Color val) { background = val; }

        private StarField starField = null;
            public StarField GetStarField() { return starField; }
            public void SetStarField(StarField val) { starField = val; }

        private Grid grid = null;
            public Grid GetGrid() { return grid; }
            public void SetGrid(Grid val) { grid = val; }

        private int positionX = 0;
            public int GetPositionX() { return positionX; }
            public void SetPositionX(int val) { positionX = val; }
            public void AddPositionX(int val) { positionX += val; }

        private int positionY = 0;
            public int GetPositionY() { return positionY; }
            public void SetPositionY(int val) { positionY = val; }
            public void AddPositionY(int val) { positionY += val; }

        private String travelToDir = "";
            public String GetTravelToDir() { return travelToDir; }
            public void SetTravelToDir(String val) { travelToDir = val; }

        private String menuType = "";
            public String GetMenuType() { return menuType; }
            public void SetMenuType(String val) { menuType = val; }

        private String hitId = "";
            public String GetHitId() { return hitId; }
            public void SetHitId(String val) { hitId = val; }

        private Level_Sub subLevel = null;
            public Level_Sub GetSubLevel() { return subLevel; }
            public void SetSubLevel(Level_Sub val) { subLevel = val; }
            public void ClearSubLevel() { subLevel = null; }
    //


    public Level(String idIn, int levelTypeIn, String dir, String parentIdIn, String metaIn)
    {
        String fil = "data/game.dat";
        String lvlDir = "data/levels";
        String lvlDat = "";

        if (idIn.equals(""))
        {
            idIn = Reusable.Functions.GetGUID();
        }

        lvlDat = lvlDir + "/" + idIn + ".dat";

        Reusable.Functions.SetSetting(fil, "level", idIn);
        Reusable.Functions.SetSetting(lvlDat, "libdir", dir);
        Reusable.Functions.SetSetting(lvlDat, "parent", parentIdIn);
        Reusable.Functions.SetSetting(lvlDat, "meta", metaIn);

        SetPositionX(Reusable.Functions.GetSetting(lvlDat, "positionx", 0));
        SetPositionY(Reusable.Functions.GetSetting(lvlDat, "positiony", 0));

        SetId(idIn);
        SetLevelMeta(metaIn);
        SetLevelType(levelTypeIn);
        SetLibraryDir(dir);

        SetupClasses();
    }

    private void SetupClasses()
    {
        String gameData = "data/game.dat";
        String conf = "config/settings.config";
        String lvlDir = "data/levels";
        String lvlDat = lvlDir + "/" + GetId() + ".dat";
        String settings = GetLibraryDir() + "/settings.config";
        String tmp = "";
        boolean useStars = false;
        boolean useGrid = false;
        String filename = "";
        Color bg = Functions.ParseColor(Functions.GetSetting(settings, "background", "0,0,0,255"));

        SetBackground(bg);

        // Setup the main movement - equal to the display size
        Move m = new Move(Game.GAMEWIDTH, Game.GAMEHEIGHT, Game.GAMEHEIGHT, Game.GAMEHEIGHT);
        SetMove(m);
        Coord c = GetMove().GetCoord();

        tmp = Functions.GetSetting(settings, "use_starfield", "0").trim().toLowerCase();
        if (Functions.StringToBoolean(tmp)) useStars = true;
        tmp = Functions.GetSetting(settings, "use_grid", "0").trim().toLowerCase();
        if (Functions.StringToBoolean(tmp)) useGrid = true;

        // Attempt to restore previous state
        String prevSecFile = Reusable.Functions.GetSetting(lvlDat, "sectorfile", "");
        if (!prevSecFile.equals(""))
        {
            GetSectors().ParseFileShift(prevSecFile);
        }

        if (Reusable.Functions.GetSetting(conf, "debug", 0) == 1)
        {
            DevStats ds = new DevStats(Color.RED);
            SetDevStats(ds);
        }

        GetSectors().SetupSectorFiles(GetId(), 0, 0);
        LoadSectors(true);

        if (GetLevelType() == LVL_SPACE)
        {
            Reusable.Functions.SetSetting(lvlDat, "leveltype", LVL_SPACE);
            SetSubLevel(new Level_Sub());

            GetSubLevel().SetSectorFile(GetSectors().GetCFile());
        }

        if (GetLevelType() == LVL_SUB)
        {
            Reusable.Functions.SetSetting(lvlDat, "leveltype", LVL_SUB);
            SetSubLevel(new Level_Sub());

            GetSubLevel().SetSectorFile(GetSectors().GetCFile());
        }

        if (useStars)
        {
            StarField sf = new StarField(GetPlayerSpeed(), GetPlayerSpeed(), Game.GAMEWIDTH, Game.GAMEHEIGHT);
            SetStarField(sf);
        }

        if (useGrid)
        {
            tmp = Functions.GetSetting(settings, "grid_color", "0,0,0,255");
            Grid gr = new Grid(Functions.ParseColor(tmp), 32);
            SetGrid(gr);
        }
    }

    public void LoadSectors(boolean initialLoad)
    {
        for (int x = -1; x < 2; x++)
        {
            for (int y = -1; y < 2; y++)
            {
                LoadSector(x, y, initialLoad);
            }
        }
    }

    public void LoadSector(int gridPosX, int gridPosY, boolean initialLoad)
    {
        Sector s = GetSectors();
        String secFile = s.GetSectorGridFile(gridPosX, gridPosY);
        if (GetSubLevel() != null && !initialLoad && GetSubLevel().SectorLoaded(secFile)) return;
        Coord c = GetMove().GetCoord();
        int x = 0;
        int y = 0;
        boolean handled = false;

        if (gridPosX == 0 && gridPosY == 0)
        {
            if (GetSubLevel() != null)
            {
                x = c.GetLeft();
                y = c.GetTop();
                GetSubLevel().Load(GetId(), GetLibraryDir(), secFile, c, x, y, GetPlayerSpeed(), initialLoad);
            }

            return;
        }

        handled = false;
        if (!handled && c.GetLeft() < -(Game.GAMEWIDTH / 2))
        {
            x = (c.GetLeft() - Game.GAMEWIDTH) - Game.GAMEWIDTH;
            x = (x * gridPosX);
            handled = true;
        }
        if (!handled && c.GetLeft() > (Game.GAMEWIDTH / 2))
        {
            x = (c.GetLeft() - Game.GAMEWIDTH) + Game.GAMEWIDTH;
            x = (x * gridPosX) - Game.GAMEWIDTH;
            handled = true;
        }
        if (handled)
        {
            c.SetLeft(0);
        }

        handled = false;
        if (!handled && c.GetTop() < -(Game.GAMEHEIGHT / 2))
        {
            y = (c.GetTop() - Game.GAMEHEIGHT) - Game.GAMEHEIGHT;
            y = (y * gridPosY);
            handled = true;
        }
        if (!handled && c.GetTop() > (Game.GAMEHEIGHT / 2))
        {
            y = (c.GetTop() - Game.GAMEHEIGHT) + Game.GAMEHEIGHT;
            y = (y * gridPosY) - Game.GAMEHEIGHT;
            handled = true;
        }
        if (handled)
        {
            c.SetTop(0);
        }

        if (x == 0 && gridPosX != 0) x = (Game.GAMEWIDTH * gridPosX);
        if (y == 0 && gridPosY != 0) y = (Game.GAMEHEIGHT * gridPosY);

        if (GetSubLevel() != null)
        {
            GetSubLevel().Load(GetId(), GetLibraryDir(), secFile, c, x, y, GetPlayerSpeed(), initialLoad);
        }
    }

    public List<String> GetAllSectorFiles()
    {
        List<String> lst = new ArrayList<String>();
        Sector s = GetSectors();

        lst.add(s.GetNwFile());
        lst.add(s.GetNFile());
        lst.add(s.GetNeFile());
        lst.add(s.GetWFile());
        lst.add(s.GetCFile());
        lst.add(s.GetEFile());
        lst.add(s.GetSwFile());
        lst.add(s.GetSFile());
        lst.add(s.GetSeFile());

        return lst;
    }

    public void SaveGame()
    {
        String gDat = "data/game.dat";
        Level_Sub sub = GetSubLevel();
        if (sub == null) return;
        Stats stat = sub.GetStats();
        if (stat == null) return;

        stat.FixStats();

        Functions.SetSetting(gDat, "minerals", stat.GetMinerals());
        Functions.SetSetting(gDat, "fuel", stat.GetFuel());
        Functions.SetSetting(gDat, "points", stat.GetScore());
    }

    public void SaveRemoved(String id)
    {
        List<String> lst = GetAllSectorFiles();
        if (GetSubLevel() == null) return;
        Level_Sub sub = GetSubLevel();

        for (String s : lst)
        {
            sub.SaveRemoved(id, s);
        }
    }

    public boolean CollisionDetection()
    {
        boolean hit = false;
        Move m = GetMove();
        Coord c = m.GetCoord();
        CollisionDetail test = null;
        if (GetSubLevel() == null) return hit;
        String attrib = "";
        String val = "";
        int valInt = 0;
        String[] valRange = null;
        String hitId = "";

        test = GetSubLevel().GetPlayer().GetCollision().CollisionCheck();

        if (test != null)
        {
            m.SetMovingX(0);
            m.SetMovingY(0);
            for (Adjustment a : test.GetCollidedWith().GetAdjustments())
            {
                // INCREMENT VALUE
                if (a.GetAdjustType() == Adjustment.ADJ_INCREMENT)
                {
                    attrib = a.GetAdjustAttrib();
                    val = a.GetAdjustValue();
                    try
                    {
                        valInt = Integer.parseInt(a.GetAdjustValue());
                    }
                    catch (Exception ex)
                    {
                        valRange = val.split(",");
                        if (valRange.length == 2)
                        {
                            try
                            {
                                valInt = Functions.RandomInt(Integer.parseInt(valRange[0].trim()), Integer.parseInt(valRange[1].trim()));
                            }
                            catch (Exception ex2) {}
                        }
                    }
                    GetSubLevel().IncrementAttribute(attrib, valInt);
                    if (a.GetRemoveElement())
                    {
                        hitId = test.GetCollidedWith().GetId();
                        GetSubLevel().RemoveElement(hitId);
                        SaveRemoved(hitId);
                        GetSubLevel().GetPlayer().GetCollision().RemoveGameObject(hitId);
                    }
                }

                // TRAVEL
                if (a.GetAdjustType() == Adjustment.ADJ_TRAVEL)
                {
                    String lvlConfig = "data/levels/" + GetId() + ".dat";

                    Functions.SetSetting(lvlConfig, "left", c.GetLeft());
                    Functions.SetSetting(lvlConfig, "top", c.GetTop());
                    Functions.SetSetting(lvlConfig, "positionx", c.GetPositionX());
                    Functions.SetSetting(lvlConfig, "positiony", c.GetPositionY());

                    String trvFl = "travel/" + test.GetCollidedWith().GetId() + ".dat";
                    File tf = new File(trvFl);

                    val = ProcessPath(test.GetCollidedWith().GetDirectory(), a.GetAdjustValue());
                    SetHitId(test.GetCollidedWith().GetId());
                    SetTravelToDir(val);
                }

                // MENU
                if (a.GetAdjustType() == Adjustment.ADJ_MENU)
                {
                    val = a.GetAdjustValue();
                    SetHitId(test.GetCollidedWith().GetId());
                    SetMenuType(val);
                }
            }
        }

        return hit;
    }


    // STANDARD METHOD HELPERS
    private void GameUpdate_DevStats()
    {
        if (GetDevStats() != null)
        {
            if (GetSubLevel() != null)
            {
                List<String> dvs = new ArrayList<String>();
                Move m = GetMove();
                Coord c = m.GetCoord();

                dvs.add("Sector : " + GetSectors().GetCFile());
                dvs.add("X, Y : " + c.GetPositionX() + ", " + c.GetPositionY());

                GetDevStats().GameUpdate(dvs);

                // GetDevStats().GameUpdate(new ArrayList<>(Arrays.asList("Elem Count: " + GetSubLevel().GetElements().size())));
            }
        }
    }


    public String ProcessPath(String selfDir, String pathIn)
    {
        pathIn = pathIn.replace("{self}", selfDir);
        return pathIn;
    }


    // STANDARD GAME METHODS
    public void KeyPressed(int keyCode)
    {
        Move m = GetMove();

        if (keyCode == KeyEvent.VK_LEFT)
        {
            m.SetMovingX(-1);
        }
        if (keyCode == KeyEvent.VK_RIGHT)
        {
            m.SetMovingX(1);
        }
        if (keyCode == KeyEvent.VK_UP)
        {
            m.SetMovingY(-1);
        }
        if (keyCode == KeyEvent.VK_DOWN)
        {
            m.SetMovingY(1);
        }

        if (GetSubLevel() != null)
        {
            GetSubLevel().SetMovingX(m.GetMovingX());
            if (GetStarField() != null)
            {
                GetStarField().SetMovingX(m.GetMovingX());
            }
            GetSubLevel().SetMovingY(m.GetMovingY());
            if (GetStarField() != null)
            {
                GetStarField().SetMovingY(m.GetMovingY());
            }
        }
    }

    public void KeyReleased(int keyCode)
    {
        Move m = GetMove();

        if (keyCode == KeyEvent.VK_LEFT ||
            keyCode == KeyEvent.VK_RIGHT)
        {
            m.SetMovingX(0);
        }

        if (keyCode == KeyEvent.VK_UP ||
            keyCode == KeyEvent.VK_DOWN)
        {
            m.SetMovingY(0);
        }

        if (GetSubLevel() != null)
        {
            GetSubLevel().SetMovingX(m.GetMovingX());
            GetSubLevel().SetMovingY(m.GetMovingY());
        }
    }

    public void GameUpdate()
    {
        Sector s = GetSectors();
        Move m = GetMove();
        Coord c = m.GetCoord();
        Level_Sub sub = null;
        String sectFile = "";
        String gameData = "data/game.dat";
        String lvlDir = "data/levels";
        String lvlDat = lvlDir + "/" + GetId() + ".dat";
        String prevSecFile = Reusable.Functions.GetSetting(lvlDat, "sectorfile", "");
        boolean initLoad = false;

        c.AddPositionX(-(m.GetMovingX()*GetPlayerSpeed()));
        c.AddPositionY(-(m.GetMovingY()*GetPlayerSpeed()));
        c.AddLeft(-(m.GetMovingX()*GetPlayerSpeed()));
        c.AddTop(-(m.GetMovingY()*GetPlayerSpeed()));

        AddPositionX(-(m.GetMovingX()*GetPlayerSpeed()));
        AddPositionY(-(m.GetMovingY()*GetPlayerSpeed()));

        GameUpdate_DevStats();

        sectFile = s.SetupCurrent(GetId(), c.GetLeft(), c.GetTop());

        if (!GetCurSector().equals(s.GetCFile()))
        {
            SaveGame();
            Reusable.Functions.SetSetting(lvlDat, "sectorfile", s.GetCFile());
            Reusable.Functions.SetSetting(lvlDat, "positionx", GetPositionX());
            Reusable.Functions.SetSetting(lvlDat, "positionx", GetPositionX());
            Reusable.Functions.SetSetting(lvlDat, "left", c.GetLeft());
            Reusable.Functions.SetSetting(lvlDat, "top", c.GetTop());

            if (!SECTORS_LOADED) initLoad = true;

            LoadSectors(initLoad);
            SECTORS_LOADED = true;
            SetCurSector(s.GetCFile());
            if (GetSubLevel() != null)
            {
                GetSubLevel().CleanElements(GetSectors());
            }
        }

        if (GetSubLevel() != null)
        {
            sub = GetSubLevel();
            CollisionDetection();

            if (m.GetMovingX() != 0 || m.GetMovingY() != 0)
            {
                if (sub.GetStats() != null && sub.GetStats().GetUseFuel())
                {
                    sub.GetStats().BurnFuel();
                }
            }

            GetSubLevel().SetSectorFile(sectFile);
            GetSubLevel().GameUpdate(m, GetPlayerSpeed(), GetPositionX(), GetPositionY());

            if (GetStarField() != null)
            {
                if (m.GetMovingX() != 0)
                {
                    for (Element e : GetStarField().GetStars())
                    {
                        e.GetMove().GetCoord().AddLeft(e.GetMove().GetSpeedX() * m.GetMovingX());
                        e.GameUpdate();
                    }
                }
                if (m.GetMovingY() != 0)
                {
                    for (Element e : GetStarField().GetStars())
                    {
                        e.GetMove().GetCoord().AddTop(e.GetMove().GetSpeedY() * m.GetMovingY());
                        e.GameUpdate();
                    }
                }
            }

            if (GetGrid() != null)
            {
                if (m.GetMovingX() != 0)
                {
                    GetGrid().AddOffsetX(m.GetMovingX() * GetPlayerSpeed());

                    if (m.GetMovingX() < 0) GetGrid().RemoveFirst(true);
                    else GetGrid().RemoveLast(true);
                }

                if (m.GetMovingY() != 0)
                {
                    GetGrid().AddOffsetY(m.GetMovingY() * GetPlayerSpeed());

                    if (m.GetMovingY() < 0) GetGrid().RemoveFirst(false);
                    else GetGrid().RemoveLast(false);
                }
            }
        }
    }

    public void GameDraw(Graphics2D g)
    {
        g.setColor(GetBackground());
        g.fillRect(0, 0, Game.GAMEWIDTH, Game.GAMEHEIGHT);

        g.setColor(new Color(0,0,0,255));

        if (GetGrid() != null)
        {
            GetGrid().GameDraw(g);
        }

        if (GetStarField() != null)
        {
            GetStarField().GameDraw(g);
        }

        if (GetSubLevel() != null)
        {
            GetSubLevel().GameDraw(g);
        }

        if (GetDevStats() != null) GetDevStats().GameDraw(g);
    }
}
