package Engine_Game;

import java.util.*;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.Color;

public class DevStats
{
    /* PROPERTIES */
    //
        private int rowHeight = 14;
            public int GetRowHeight() { return rowHeight; }

        private int padding = 5;
            public int GetPadding() { return padding; }

        private int columnWidth = 300;
            public int GetColumnWidth() { return columnWidth; }

        private Color color;
            public Color GetColor() { return color; }
            public void SetColor(Color val) { color = val; }

        private List<String> messages = null;
            public List<String> GetMessages() { if (messages == null) messages = new ArrayList<String>(); return messages; }
            public void SetMessages(List<String> val) { messages = val; }
            public void AddMessage(String val) { GetMessages().add(val); }
            public void ClearMessages() { messages = null; }
    //


    public DevStats(Color colorIn)
    {
        SetColor(colorIn);
    }


    public void GameUpdate(List<String> messagesIn)
    {
        SetMessages(messagesIn);
    }

    public void GameDraw(Graphics2D g)
    {
        int y = GetRowHeight() * 2;
        int x = GetRowHeight();

        g.setColor(GetColor());
        g.setFont(new Font("Century Gothic", Font.PLAIN, GetRowHeight()));

        for (String s : GetMessages())
        {
            g.drawString(s, x, y);
            y += GetRowHeight() + GetPadding();
        }

        g.setColor(Color.BLACK);
    }
}
