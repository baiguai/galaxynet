package Engine_Game;

import java.io.*;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.image.*;
import java.lang.Math.*;
import java.util.*;
import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import Reusable.Functions;

public class Element
{
    /* PROPERTIES */
    //
        /* ENUMS */
        //
            public static int ELEM_STAR = 1;
            public static int ELEM_GRID = 10;
            public static int ELEM_IMAGE = 20;
        //

        private String id = "";
            public String GetId() { return id; }
            public void SetId(String val) { id = val; }

        private String directory = "";
            public String GetDirectory() { return directory; }
            public void SetDirectory(String val) { directory = val; }

        private String sectorFile = "";
            public String GetSectorFile() { return sectorFile; }
            public void SetSectorFile(String val) { sectorFile = val; }

        private int elementType = 1;
            public int GetElementType() { return elementType; }
            public void SetElementType(int val) { elementType = val; }

        private String gameElementType = "";
            public String GetGameElementType() { return gameElementType; }
            public void SetGameElementType(String val) { gameElementType = val; }

        private List<Adjustment> adjustments = null;
            public List<Adjustment> GetAdjustments() { if (adjustments == null) adjustments = new ArrayList<Adjustment>(); return adjustments; }
            public void SetAdjustments(List<Adjustment> val) { adjustments = val; }
            public void AddAdjustment(Adjustment val) { GetAdjustments().add(val); }
            public void ClearAdjustments() { adjustments = null; }

        private boolean doCollide = true;
            public boolean GetDoCollide() { return doCollide; }
            public void SetDoCollide(boolean val) { doCollide = val; }

        private Collision collision = null;
            public Collision GetCollision() { if (collision == null && GetDoCollide()) collision = new Collision(this); return collision; }
            public void SetCollision(Collision val) { collision = val; }

        private Color color = null;
            public Color GetColor() { return color; }
            public void SetColor(Color val) { color = val; }

        private String label = "";
            public String GetLabel() { return label; }
            public void SetLabel(String val) { label = val; }

        private boolean centerElement = false;
            public boolean GetCenterElement() { return centerElement; }
            public void SetCenterElement(boolean val) { centerElement = val; }

        private Move move = null;
            public Move GetMove() { return move; }
            public void SetMove(Move val) { move = val; }

        private boolean useSprite = false;
            public boolean GetUseSprite() { return useSprite; }
            public void SetUseSprite(boolean val) { useSprite = val; }

        private String imagePath = "";
            public String GetImagePath() { return imagePath; }
            public void SetImagePath(String val) { imagePath = val; }

        private BufferedImage bufImage = null;
            public BufferedImage GetBufImage() { return bufImage; }
            public void SetBufImage(BufferedImage val) { bufImage = val; }

        private List<Element> upgrades = null;
            public List<Element> GetUpgrades() { if (upgrades == null) upgrades = new ArrayList<Element>(); return upgrades; }
            public void SetUpgrades(List<Element> val) { upgrades = val; }
            public void AddUpgrade(Element val) { GetUpgrades().add(val); }
            public void ClearUpgrades() { upgrades = null; }

    //


    public Element(String idIn, int elementTypeIn, String gameElementTypeIn, String directoryIn, int widthIn, int heightIn, int gameWidthIn, int gameHeightIn, boolean doCollideIn)
    {
        if (idIn.equals(""))
        {
            idIn = Reusable.Functions.GetGUID();
        }

        SetId(idIn);

        SetElementType(elementTypeIn);
        SetGameElementType(gameElementTypeIn);
        SetDirectory(directoryIn);
        SetDoCollide(doCollideIn);
        Move m = new Move(widthIn, heightIn, gameWidthIn, gameHeightIn);
        SetMove(m);

        if (elementTypeIn != ELEM_STAR && !gameElementTypeIn.equals("player"))
        {
            LoadAdjustments();
        }
    }


    private void LoadAdjustments()
    {
        String dat = "data/adjustments/" + GetId() + ".dat";
        File f = new File(dat);
        List<String> data = null;
        List<String> attData = null;
        String[] arr = null;
        Adjustment a = null;
        String dataRow = "";

        String id = "";
        int adjTp = 0;
        String adjAtt = "";
        String adjVal = "";
        boolean rmv = false;


        ClearAdjustments();

        if (f.exists())
        {
            // Attributes have been stored - load them
            data = Reusable.Functions.ReadFile(dat);

            /*
                Structure:
                - id
                - adjustmenttype
                - adjustmentattribute
                - adjustmentvalue
                - removeelement
            */

            for (String r : data)
            {
                if (r.equals("")) continue;
                r = r.replace("|", "@@@");
                arr = r.split("@@@");

                if (arr.length == 5)
                {
                    id = arr[0].trim();
                    adjTp = Integer.parseInt(arr[1].trim());
                    adjAtt = arr[2].trim();
                    adjVal = arr[3].trim();
                    rmv = false;
                    if (arr[4].trim().equals("1") || arr[4].trim().toLowerCase().equals("true"))
                    {
                        rmv = true;
                    }

                    a = new Adjustment(id, adjTp, adjAtt, adjVal, rmv);
                    AddAdjustment(a);
                }
            }
        }
        else
        {
            // No attributes stored - build them
            String settings = GetDirectory() + "/settings.config";
            attData = new ArrayList<String>();

            id = "";
            f = new File(settings);

            if (f.exists())
            {
                data = Reusable.Functions.ReadFile(settings);

                /*
                    Structure:
                    - "adjust"
                    - adjustmenttype
                    - adjustmentattribute
                    - adjustmentvalue
                    - removeelement
                */

                for (String r : data)
                {
                    if (r.equals("")) continue;
                    r = r.replace("|", "@@@");
                    arr = r.split("@@@");

                    if (arr.length == 5 && arr[0].trim().toLowerCase().equals("adjust"))
                    {
                        adjTp = Integer.parseInt(arr[1].trim());
                        adjAtt = arr[2].trim();
                        adjVal = arr[3].trim();
                        rmv = false;
                        if (arr[4].trim().equals("1"))
                        {
                            rmv = true;
                        }
                        a = new Adjustment(id, adjTp, adjAtt, adjVal, rmv);
                        AddAdjustment(a);

                        dataRow = a.GetId() + "|" + a.GetAdjustType() + "|" + a.GetAdjustAttrib() + "|" + a.GetAdjustValue() + "|" + a.GetRemoveElement();
                        attData.add(dataRow);
                    }
                }

                Reusable.Functions.WriteToFile(dat, attData);
            }
        }
    }

    public void SaveAdjustments()
    {
        String dirDat = "data/adjustments/" + GetId() + ".dat";
    }


    public void SetImage(String imagePathIn)
    {
        SetImagePath(imagePathIn);

        File f = new File(GetImagePath());
        BufferedImage imageIn = null;

        try
        {
            if (!f.equals(""))
            {
                Coord c = GetMove().GetCoord();
                imageIn = ImageIO.read(f);
                SetBufImage(imageIn);

                if (!GetUseSprite())
                {
                    c.SetWidth(imageIn.getWidth());
                    c.SetHeight(imageIn.getHeight());
                }
            }
        }
        catch (Exception ex)
        {
            System.out.println("Can't load image: " + GetImagePath());
            ex.printStackTrace();
            return;
        }
    }


    // MOVEMENT METHODS
    public void SetMovingX(int moving)
    {
        GetMove().SetMovingX(moving);
    }
    public void SetMovingY(int moving)
    {
        GetMove().SetMovingY(moving);
    }
    public void StopX()
    {
        SetMovingX(0);
    }
    public void StopY()
    {
        SetMovingY(0);
    }



    public void GameUpdate()
    {
        Move m = GetMove();
        Coord c = null;
        int padding = 20;

        if (m.GetWrap())
        {
            c = m.GetCoord();
            if (c.GetLeft() > (Game.GAMEWIDTH + padding))
            {
                if (m.GetRandomOnWrap())
                {
                    c.SetTop(Reusable.Functions.RandomInt(0, Game.GAMEHEIGHT));
                }

                c.SetLeft(-(padding));
            }
            if (c.GetLeft() < -padding)
            {
                if (m.GetRandomOnWrap())
                {
                    c.SetTop(Reusable.Functions.RandomInt(0, Game.GAMEHEIGHT));
                }

                c.SetLeft(Game.GAMEWIDTH + padding);
            }
            if (c.GetTop() > (Game.GAMEHEIGHT + padding))
            {
                if (m.GetRandomOnWrap())
                {
                    c.SetLeft(Reusable.Functions.RandomInt(0, Game.GAMEWIDTH));
                }

                c.SetTop(-(padding));
            }
            if (c.GetTop() < -padding)
            {
                if (m.GetRandomOnWrap())
                {
                    c.SetLeft(Reusable.Functions.RandomInt(0, Game.GAMEWIDTH));
                }

                c.SetTop(Game.GAMEHEIGHT + padding);
            }
        }
    }

    public void GameDraw(Graphics2D g, String label)
    {
        Coord c = GetMove().GetCoord();
        Color clr = new Color(255,255,255,255);
        if (GetColor() != null) clr = GetColor();
        int top = 0;

        if (GetElementType() == ELEM_STAR)
        {
            g.setColor(clr);
            g.fillRect(c.GetLeft(), c.GetTop(), c.GetWidth(), c.GetHeight());
        }

        if (GetElementType() == ELEM_IMAGE)
        {
            if (GetBufImage() != null)
            {
                if (GetUseSprite())
                {
                    int dir = 0;

                    if (GetMove().GetMovingX() < 0) dir = 3;
                    if (GetMove().GetMovingX() > 0) dir = 1;
                    if (GetMove().GetMovingY() < 0) dir = 0;
                    if (GetMove().GetMovingY() > 0) dir = 2;

                    g.drawImage(GetBufImage().getSubimage((dir * c.GetWidth()), 0, c.GetWidth(), c.GetHeight()), c.GetLeft(), c.GetTop(), null);

                    for (Element e : GetUpgrades())
                    {
                        if (e.GetBufImage() != null)
                        {
                            g.drawImage(e.GetBufImage().getSubimage((dir * c.GetWidth()), 0, c.GetWidth(), c.GetHeight()), c.GetLeft(), c.GetTop(), null);
                        }
                    }
                }
                else
                {
                    if (!GetCenterElement())
                    {
                        g.drawImage(GetBufImage(), c.GetLeft(), c.GetTop(), null);
                    }
                    else
                    {
                        g.drawImage(GetBufImage(), (Game.GAMEWIDTH / 2) - (GetBufImage().getWidth() / 2), (Game.GAMEHEIGHT / 2) - (GetBufImage().getHeight() / 2), null);
                    }
                }
            }

            top = c.GetTop() + c.GetHeight();

            if (!GetLabel().equals(""))
            {
                top += 24;
                g.setColor(clr);
                g.setFont(new Font("Century Gothic", Font.PLAIN, 12));
                g.drawString(Reusable.Functions.TitleCase(GetLabel()), c.GetLeft(), top);
            }
        }
    }
}
