package Engine_Game;

import java.awt.Color;
import java.io.*;
import java.util.*;
import java.awt.Graphics2D;
import java.awt.image.*;
import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import Reusable.Functions;

public class MenuItem
{
    /* PROPERTIES */
    //
        private String settingsFile = "";
            public String GetSettingsFile() { return settingsFile; }
            public void SetSettingsFile(String val) { settingsFile = val; }

        private String imagePath = "";
            public String GetImagePath() { return imagePath; }
            public void SetImagePath(String val) { imagePath = val; }

        private BufferedImage image = null;
            public BufferedImage GetImage() { return image; }
            public void SetImage(BufferedImage val) { image = val; }

        private String label = "";
            public String GetLabel() { return label; }
            public void SetLabel(String val) { label = val; }
    //


    public MenuItem(String rootDirIn)
    {
        SetSettingsFile(rootDirIn + "/settings.config");
        SetImagePath(rootDirIn + "/thumbnail.png");

        SetupClasses();
    }

    private void SetupClasses()
    {
        SetImage(Functions.GetImage(GetImagePath()));
    }


    public void GameUpdate()
    {}

    public void GameDraw(Graphics2D g, int x, int y)
    {
        g.drawImage(GetImage(), x, y, null);
    }
}
