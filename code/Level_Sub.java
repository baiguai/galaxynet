package Engine_Game;

import javax.swing.*;
import java.awt.Color;
import java.awt.Graphics2D;
import java.io.*;
import java.io.IOException;
import java.util.*;
import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.awt.Dimension;
import Reusable.Functions;

public class Level_Sub
{
    /* PROPERTIES */
    //
        private boolean AdjustingElements = false;

        private String parentId = "";
            public String GetParentId() { return parentId; }
            public void SetParentId(String val) { parentId = val; }

        private String libraryDir = "";
            public String GetLibraryDir() { return libraryDir; }
            public void SetLibraryDir(String val) { libraryDir = val; }

        private int playerSpeed = 4;
            public int GetPlayerSpeed() { return playerSpeed; }
            public void SetPlayerSpeed(int val) { playerSpeed = val; }

        private String sectorFile = "";
            public String GetSectorFile() { return sectorFile; }
            public void SetSectorFile(String val) { sectorFile = val; }

        private Element player = null;
            public Element GetPlayer() { return player; }
            public void SetPlayer(Element val) { player = val; }

        private List<Element> elements = null;
            public List<Element> GetElements() { if (elements == null) elements = new ArrayList<Element>(); return elements; }
            public Element GetElement(String id)
            {
                AdjustingElements = true;
                Element output = null;

                for (Element e : GetElements())
                {
                    if (id.equals(e.GetId()))
                    {
                        output = e;
                        break;
                    }
                }

                AdjustingElements = false;
                return output;
            }
            public void SetElements(List<Element> val) { elements = val; }
            public void AddElement(Element val)
            {
                if (GetElement(val.GetId()) == null)
                {
                    GetElements().add(val);
                }
            }
            public void ClearElements() { elements = null; }
            public void RemoveElement(String id)
            {
                List<Element> output = new ArrayList<Element>();
                AdjustingElements = true;

                for (Element e : GetElements())
                {
                    if (!e.GetId().equals(id))
                    {
                        output.add(e);
                    }
                }

                SetElements(output);
                AdjustingElements = false;
            }
            public void CleanElements(Sector s)
            {
                List<Element> output = new ArrayList<Element>();
                AdjustingElements = true;
                Coord c = null;

                for (Element e : GetElements())
                {
                    if (
                        e.GetSectorFile().equals(s.GetNwFile()) ||
                        e.GetSectorFile().equals(s.GetNFile()) ||
                        e.GetSectorFile().equals(s.GetNeFile()) ||
                        e.GetSectorFile().equals(s.GetWFile()) ||
                        e.GetSectorFile().equals(s.GetCFile()) ||
                        e.GetSectorFile().equals(s.GetEFile()) ||
                        e.GetSectorFile().equals(s.GetSwFile()) ||
                        e.GetSectorFile().equals(s.GetSFile()) ||
                        e.GetSectorFile().equals(s.GetSeFile())
                    )
                    {
                        output.add(e);
                    }
                    else
                    {
                        // Ensure that nothing in view gets removed
                        c = e.GetMove().GetCoord();

                        if (c.InView())
                        {
                            output.add(e);
                        }
                    }
                }

                SetElements(output);
                AdjustingElements = false;
            }
            public boolean SectorLoaded(String secFile)
            {
                boolean output = false;

                for (Element e : GetElements())
                {
                    if (e.GetSectorFile().equals(secFile))
                    {
                        output = true;
                        break;
                    }
                }

                return output;
            }

        private Stats stats = null;
            public Stats GetStats() { return stats; }
            public void SetStats(Stats val) { stats = val; }
    //


    public void Load(String parentIdIn, String libDirRoot, String sectorFileIn, Coord c, int leftIn, int topIn, int playerSpeedIn, boolean initialLoad)
    {
        SetParentId(parentIdIn);
        SetSectorFile(sectorFileIn);
        Element e = null;
        int min = 0;
        int max = 0;
        int count = 0;
        int adj = 0;
        String dir = "data/sectors/";
        String libDir = "";
        String config = libDirRoot + "/settings.config";
        String[] types = Reusable.Functions.GetSetting(config, "types", "").split(",");
        List<String> dirs = null;
        String typeDir = "";
        File f = new File(dir + sectorFileIn);
        List<String> secData = null;
        String dataRow = "";
        String[] arr = null;
        String label = "";
        String color = "";
        boolean usePlx = Functions.StringToBoolean(Functions.GetSetting(config, "use_parallax", "0"));

        SetLibraryDir(libDirRoot);

        /*

            SecData Schema:
            - 'elem'
            - id
            - type
            - gameelementtype
            - typedir
            - imagepath
            - left
            - top
            - spdadj
            - label

        */

        if (f.exists())
        {
            secData = Reusable.Functions.ReadFile(dir + sectorFileIn);
        }

        if (secData == null || secData.size() < 1)
        {
            secData = new ArrayList<String>();
            for (String s : types)
            {
                // FOR EACH TYPE - PICK A RANDOM DIRECTORY
                libDir = libDirRoot + "/" + s;
                dirs = Functions.ListDirs(libDir);
                min = Functions.GetSetting(config, s + "_min", 0);
                max = Functions.GetSetting(config, s + "_max", 0);
                label = Functions.GetSetting(config, s + "_label", "");
                color = Functions.GetSetting(config, "label_color", "255,255,255,255");

                for (int i = 0; i < Functions.RandomInt(min, max); i++)
                {
                    if (dirs.size() > 0)
                    {
                        adj = Functions.RandomInt(0, 2);
                        if (!usePlx) adj = 0;
                        typeDir = libDir + "/" + dirs.get(Reusable.Functions.RandomInt(0, dirs.size()-1));
                        e = new Element("", Element.ELEM_IMAGE, s, typeDir, 8, 8, Game.GAMEWIDTH, Game.GAMEHEIGHT, true);
                        e.SetImagePath(typeDir + "/" + s + ".png");
                        e.SetImage(e.GetImagePath());
                        e.GetMove().SetSpeedX(playerSpeedIn + adj);
                        e.GetMove().SetSpeedY(playerSpeedIn + adj);
                        e.SetLabel(label);
                        e.SetColor(Functions.ParseColor(color));
                        c = e.GetMove().GetCoord();
                        c.SetLeft(Reusable.Functions.RandomInt(0, Game.GAMEWIDTH) + leftIn);
                        c.SetTop(Reusable.Functions.RandomInt(0, Game.GAMEHEIGHT) + topIn);
                        e.SetSectorFile(sectorFileIn);
                        AddElement(e);
                    }
                }
            }

            SaveSector();
        }
        else
        {
            for (String r : secData)
            {
                r = r.replace("|", "@@@");
                arr = r.split("@@@");

                if (arr.length == 10)
                {
                    try
                    {
                        adj = Integer.parseInt(arr[8].trim());
                    }
                    catch (Exception ex)
                    {
                        adj = 0;
                    }

                    e = new Element(arr[1].trim(), Integer.parseInt(arr[2].trim()), arr[3].trim(), arr[4].trim(), 8, 8, Game.GAMEWIDTH, Game.GAMEHEIGHT, true);
                    e.SetImagePath(arr[5].trim());
                    e.SetImage(e.GetImagePath());
                    e.GetMove().SetSpeedX(playerSpeedIn + adj);
                    e.GetMove().SetSpeedY(playerSpeedIn + adj);
                    e.SetLabel(arr[8].trim());
                    e.SetColor(Functions.ParseColor(arr[9].trim()));
                    c = e.GetMove().GetCoord();
                    c.SetLeft(Integer.parseInt(arr[6].trim()));
                    c.SetTop(Integer.parseInt(arr[7].trim()));

                    e.SetSectorFile(sectorFileIn);

                    if (initialLoad || !c.InView())
                    {
                        AddElement(e);
                    }
                }
            }
        }

        SetupClasses();
    }

    public void SaveSector()
    {
        List<String> secData = new ArrayList<String>();
        String dir = "data/sectors/";
        String dataRow = "";
        Coord c = null;

        File f = new File(GetSectorFile());
        if (f.exists())
        {
            f.delete();
        }

        for (Element e : GetElements())
        {
            c = e.GetMove().GetCoord();
            dataRow = "elem|" + e.GetId() + "|" + e.GetElementType() + "|" + e.GetGameElementType() + "|" + e.GetDirectory() + "|" + e.GetImagePath() + "|" + c.GetLeft() + "|" + c.GetTop() + "|" + e.GetLabel() + "|" + e.GetColor();
            secData.add(dataRow);
        }

        Functions.WriteToFile(dir + GetSectorFile(), secData);
    }

    public void SaveRemoved(String id, String sectorFileIn)
    {
        String dir = "data/sectors/";
        List<String> fdata = new ArrayList<String>();
        List<String> secData = new ArrayList<String>();
        String[] arr = null;

        fdata = Functions.ReadFile(dir + sectorFileIn);

        for (String row : fdata)
        {
            arr = null;
            if (row.equals("")) continue;

            row = row.replace("|", "@@@");
            arr = row.split("@@@");

            if (!id.equals(arr[1].trim()))
            {
                secData.add(row);
            }
        }

        for (String r : secData)
        {
            r = r.replace("|", "@@@");
            arr = r.split("@@@");
        }

        Functions.OverwriteToFile(dir + sectorFileIn, secData);
    }

    private void SetupClasses()
    {
        Element e = null;
        String config = GetLibraryDir() + "/settings.config";
        String playerDir = GetLibraryDir() + "/player";
        String playerImg = playerDir + "/player.png";
        String PlayerImage = "";
        boolean burnFuel = Functions.StringToBoolean(Functions.GetSetting(config, "burn_fuel", "1"));
        Color stClr = Functions.ParseColor(Functions.GetSetting(config, "stats_color", "255,255,255,255"));

        // PLAYER
        e = new Element("", Element.ELEM_IMAGE, "player", playerDir, 24, 24, Game.GAMEWIDTH, Game.GAMEHEIGHT, true);
        e.GetMove().GetCoord().SetLeft((Game.GAMEWIDTH / 2) - 8);
        e.GetMove().GetCoord().SetTop((Game.GAMEHEIGHT / 2) - 8);
        e.GetMove().SetSpeedX(0);
        e.GetMove().SetSpeedY(0);
        e.SetUseSprite(true);
        e.SetImage(playerImg);
        e.SetElementType(Element.ELEM_IMAGE);
        SetPlayer(e);

        SetupCollision();

        Stats st = new Stats(stClr, burnFuel, true);
        SetStats(st);
    }

    private void SetupCollision()
    {
        if (GetPlayer() != null && GetPlayer().GetCollision() != null)
        {
            GetPlayer().GetCollision().ClearGameObjects();
            for (Element elem : GetElements())
            {
                if (elem.GetDoCollide())
                {
                    GetPlayer().GetCollision().AddGameObject(elem);
                }
            }
        }
    }

    public void SetMovingX(int moving)
    {
        Move m = null;

        if (GetPlayer() != null)
        {
            m = GetPlayer().GetMove();
            if (moving > 0) m.SetMovingX(-moving);
            if (moving < 0) m.SetMovingX(1);
            if (moving != 0) m.SetMovingY(0);
        }
    }

    public void SetMovingY(int moving)
    {
        Move m = null;

        if (GetPlayer() != null)
        {
            m = GetPlayer().GetMove();
            if (moving > 0) m.SetMovingY(-moving);
            if (moving < 0) m.SetMovingY(1);
            if (moving != 0) m.SetMovingX(0);
        }
    }


    public void IncrementAttribute(String attribute, int incVal)
    {
        attribute = attribute.toLowerCase();

        if (attribute.equals("fuel") && GetStats() != null)
        {
            GetStats().AddFuel(incVal);
        }

        if (attribute.equals("minerals") && GetStats() != null)
        {
            GetStats().AddMinerals(incVal);
        }
    }


    public void GameUpdate(Move m, int speed, int posX, int posY)
    {
        Coord c = m.GetCoord();
        Move plM = null;
        int speedX = 0;
        int speedY = 0;
        String fil = "data/game.dat";

        // c.SetPositionX((m.GetMovingX()*speed));
        // c.SetPositionY((m.GetMovingY()*speed));

        if (!AdjustingElements)
        {
            for (Element e : GetElements())
            {
                speedX = e.GetMove().GetSpeedX();
                speedY = e.GetMove().GetSpeedY();
                c = e.GetMove().GetCoord();
                c.AddLeft(m.GetMovingX()*speedX);
                c.AddTop(m.GetMovingY()*speedY);
            }
        }

        if (GetStats() != null)
        {
            GetStats().GameUpdate(posX, posY);
        }

        if (!AdjustingElements)
        {
            c.AddPositionX(-(m.GetMovingX())*speedX);
            c.AddPositionY(-(m.GetMovingY())*speedY);
        }
    }

    public void GameDraw(Graphics2D g)
    {
        for (Element e: GetElements())
        {
            e.GameDraw(g, e.GetLabel());
        }

        if (GetPlayer() != null)
        {
            GetPlayer().GameDraw(g, "");
        }

        if (GetStats() != null)
        {
            GetStats().GameDraw(g);
        }
    }
}
